name := "IReport"
 
version := "1.0" 
      
lazy val `ireport` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
      
scalaVersion := "2.12.2"

libraryDependencies ++= Seq( jdbc , ehcache , ws , specs2 % Test , guice, jdbc, "com.typesafe.play" %% "anorm" % "2.5.3" )

libraryDependencies += "mysql" % "mysql-connector-java" % "8.0.13"

libraryDependencies += "com.google.api-client" % "google-api-client" % "1.22.0"
libraryDependencies += "com.google.oauth-client" % "google-oauth-client" % "1.22.0"
libraryDependencies += "com.google.apis" % "google-api-services-drive" % "v3-rev107-1.22.0"
libraryDependencies += "net.glxn" % "qrgen" % "1.4"
libraryDependencies += "com.google.code.gson" % "gson" % "2.2.4"
libraryDependencies += "com.googlecode.json-simple" % "json-simple" % "1.1"
libraryDependencies += "org.apache.poi" % "poi" % "3.8"
libraryDependencies += "org.apache.poi" % "poi-ooxml" % "3.8"
libraryDependencies += "org.mindrot" % "jbcrypt" % "0.3m"




unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

      