$( document ).ready(function() {
    var transaction_id = $('#transaction_id').data('data');
    var districts = $('#json_districts').data('data');
    var wards = $('#json_wards').data('data');

    $('#select_district').on('change', function () {
        fillwards();
    });

    function emailStatus(status) {
        if(status === 1) return "delivered";
        if(status === 2) return "open";
        if(status === 3) return "dropped";
        if(status === 4) return "deferred";
        if(status === 5) return "bounce";
        if(status === 6) return "spam report";
    }


    $('.resendEmail').on('click', function () {
        let status = $('.resendEmail:eq(' + $('.resendEmail').index(this) +  ')').data("data");
        console.log(status);
        resend(status);
    });

    function resend(email_type){
        console.log(email_type, 20);

        var data = {
            id : transaction_id,
            email_phase: email_type
        };

        $.ajax({
            type: "POST",
            url: "/transaction/resend",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                console.log(data);
                if(data > 0){
                    $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
                        location.reload();
                        //$(".alert-success").slideUp(500);
                    });
                }
                //notice: success
                //reset form data
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    }

    function fillwards() {
        var district = $('#select_district').val();
        var options = '';
        for(var i = 0 ; i < wards.length; i++){
            if(wards[i].district_id == district) {
                options += '<option value="' + wards[i].id + '" ' + (i == 0 ? 'selected' : '') + '>' + wards[i].name + '</option>';
            }
        }
        $('#select_ward').empty().append(options);
    }
    fillwards();

    $('#updateStatus').on('click', function () {
        var data = {
            status: $('#status').val(),
            id : transaction_id
        };

        $.ajax({
            type: "POST",
            url: "/transaction/updateTransactionStatus",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                console.log(data);
                if(data > 0){
                    $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
                        location.reload();
                        //$(".alert-success").slideUp(500);
                    });
                }
                //notice: success
                //reset form data
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    });

    $('#updateNote').on('click', function () {
        var data = {
            note: $('#note').val(),
            id : transaction_id
        };

        $.ajax({
            type: "POST",
            url: "/transaction/updateTransactionNote",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                if(data > 0){
                    $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
                        location.reload();
                        //$(".alert-success").slideUp(500);
                    });
                }
                //notice: success
                //reset form data
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    });

    $('#updateInfo').on('click', function () {
        var data = {
            id: transaction_id,
            name: $('#name').val(),
            email: $('#email').val(),
            phone : $('#phone').val(),
            address : $('#transaction_address').val(),
            city : $('#select_city').val(),
            district : $('#select_district').val(),
            ward : $('#select_ward').val(),
        };

        console.log(data);

        $.ajax({
            type: "POST",
            url: "/transaction/updateTransactionUserInfo",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                if(data > 0){
                    $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
                        location.reload();
                        //$(".alert-success").slideUp(500);
                    });
                }
                //notice: success
                //reset form data
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    });
});
