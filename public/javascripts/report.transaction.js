$( document ).ready(function() {
    var map_pay_type = ["Thanh toán tại quầy", "Thanh toán tại quầy", "Chuyển khoản", "Visa/Master", "COD", "Thanh toán tại quầy", "Mua vé trực tiếp", "ATM", "Voucher Code", "Thanh Toán MoMo"],
        map_transation_status = ["Đã đặt chỗ", "Đã xác nhận", "Đã đóng gói", "Đang vận chuyển", "Thành công", "Thanh toán thất bại", "Hủy đơn hàng", "Đã hoàn tiền", "Đang xử lý"],
        map_transaction_status_style = ["green_report", "green_report", "green_report", "green_report", "", "red_report", "red_report", "red_report"];
    function setTime(start_item, end_item) {
        $(start_item).datetimepicker({
            format: 'MM/DD/YYYY',
            widgetPositioning: {
                vertical : 'top'
            }
        });
        $(end_item).datetimepicker({
            useCurrent: false, //Important! See issue #1075
            format: 'MM/DD/YYYY',
            widgetPositioning: {
                vertical : 'top'
            }
        });

        $(start_item).on("dp.change", function (e) {
            //$(end_item).data("DateTimePicker").minDate(e.date);
            //$(start_item).data("DateTimePicker").hide();
            //$(end_item).data("DateTimePicker").show();
        });

        $(end_item).on("dp.change", function (e) {
            //$(end_item).data("DateTimePicker").hide();
        });
    }

    setTime('#startTime', '#endTime');
    $('#filter_event').on('change', function (e) {
        location.href = './' + e.target.value;
    });

    var data = $('#json_data').data('data');
    var tt = $('#json_ticket_type').data('data');
    var districts = [], ticket_types = [];
    var filter_text = '-1', filter_ticket_type = '-1', filter_pay_type = '-1', filter_transaction_status = -1;
    var total_transaction = 0, current_transaction = 0, total_ticket = 0, current_ticket = 0, total_rev = 0, current_rev = 0, total_shipment = 0, current_shipment = 0;
    $('#json_data_district').data('data').forEach(function (d) { districts[d.id] = d.shipment_price });

    var permission_group_id = $('#permission_group_id').data('data');
    for(var i = 0 ; i < tt.length; i++){
        ticket_types[tt[i].id] = tt[i];
        ticket_types[tt[i].id]['_total_ticket'] = 0;
        ticket_types[tt[i].id]['_current_ticket'] = 0;
    }

    var min_time = 9999999999999, max_time = -1;
    for(var i = 0 ; i < data.length; i++){
        if(data[i].time_create > max_time) max_time = data[i].time_create;
        if(data[i].time_create < min_time) min_time = data[i].time_create;

        data[i].ticket_info = JSON.parse(data[i].ticket_info);
        try {
            data[i].apply_voucher = JSON.parse(data[i].apply_voucher);
        }
        catch(err) {
            data[i].apply_voucher = [];
        }

        var row = '<tr class="element ' + map_transaction_status_style[data[i].status] + '"><td><input type="checkbox"></td>';
        row += '<td>' + map_transation_status[data[i].status] + '</td>';
        if(Number(permission_group_id) === 4){
            row += '<td><p style="text-decoration: underline"><strong>#' + data[i].id + '</strong></p><p>Đặt vé lúc:</p>' +
                '<p>' + moment(data[i].time_create).format("ddd MM/DD/YYYY HH:mm:ss") + '</p></td>';
        }else {
            row += '<td><p><a style="text-decoration: underline" href="/transaction/detail/' + data[i].id + '"><strong>#' + data[i].id + '</strong></a></p><p>Đặt vé lúc:</p>' +
                '<p>' + moment(data[i].time_create).format("ddd MM/DD/YYYY HH:mm:ss") + '</p></td>';
        }

        row += '<td><h5>' + data[i].name + '</h5><p>' + data[i].email + '</p><p>' + data[i].phone + '</p><p>' + data[i].address + '</p></td>';
        row += '<td class="center" style="text-align: left">';
        var cell = '';
        var c = 0;

        for(var j = 0 ; j < data[i].ticket_info.length; j++){
            c += data[i].ticket_info[j].number;
            cell += '<p>' + data[i].ticket_info[j].name_vi_ticket_type + ' :' + data[i].ticket_info[j].number + '</p>';
            var seats_info = data[i].ticket_info[j].seats_info ? JSON.parse(data[i].ticket_info[j].seats_info) : [];

            var seats_info_string = '';
            for(k = 0 ; k < seats_info.length; k++){
                seats_info_string += seats_info_string.length == 0 ? seats_info[k].seat_name_vi : (', ' + seats_info[k].seat_name_vi);
            }
            if(seats_info_string.length > 0) cell += '<p>' + seats_info_string + '</p>';

            //other data
            if(data[i].status <= 4) {
                total_ticket += data[i].ticket_info[j].number;
                current_ticket += data[i].ticket_info[j].number;
                ticket_types[data[i].ticket_info[j].id_ticket_type]._total_ticket += data[i].ticket_info[j].number;
                ticket_types[data[i].ticket_info[j].id_ticket_type]._current_ticket += data[i].ticket_info[j].number;
            }
        }

        row += '<p>Tổng số vé: ' + c + '</p>' + cell + '</td>';
        row += '<td class="center" style="text-align: left">';
        row += (typeof(data[i].apply_voucher[0]) === 'undefined' || typeof(data[i].apply_voucher[0].voucher_code) === 'undefined') ? '' : ('<p>Code: ' + data[i].apply_voucher[0].voucher_code + '</p>');
        row += '<p>' + (data[i].revenue_discount > 0 ? ('Discount value: ' + data[i].revenue_discount.toLocaleString()) : '0') + '</p>';
        row += '</td>';
        row += '<td class="center"><p>' + data[i].revenue_after.toLocaleString() + '</p></td>';
        row += '<td>' + map_pay_type[data[i].pay_type] + '</td>';
        row += '<td>' + data[i].note + '</td>';
        $('#dataTransaction').append(row);
        //other data
        if(data[i].status > 4)
            continue;

        total_transaction++;
        current_transaction++;
        total_rev += data[i].revenue_after;
        current_rev += data[i].revenue_after;
        if(data[i].pay_type == 4){
            total_shipment += districts[data[i].dist];
            current_shipment += districts[data[i].dist];
        }
    }

    var elements = $('.element');

    $("#startTime").data("DateTimePicker").defaultDate(new Date(min_time));
    $("#endTime").data("DateTimePicker").defaultDate(new Date(max_time));

    function setData() {
        $('#total_rev').html(current_rev.toLocaleString());
        $('#total_soldticket').html(current_ticket.toLocaleString());
        $('#total_transaction').html(current_transaction.toLocaleString());
        $('#total_shipment').html(current_shipment.toLocaleString());
    }

    setData();
    doFilter();
    function doFilter() {
        var start_time = new Date($("#startTime").data('date')).getTime();
        var end_time = new Date($("#endTime").data('date')).getTime();
        current_rev = total_rev;
        current_transaction = total_transaction;
        current_ticket = total_ticket;
        current_shipment = total_shipment;
        var is_admin = parseInt($('#is_admin').data('data'));

        for(var i = 0 ; i < tt.length; i++){
            ticket_types[tt[i].id]._current_ticket = ticket_types[tt[i].id]._total_ticket;
        }
        for(var i = 0 ; i < data.length; i++){
            var index = moment(data[i].time_create).startOf('day').unix() * 1000;
            var _ele = $(elements[i]);
            var hide = false;

            _ele.show();
            if(filter_text == '-1' && filter_ticket_type == '-1' && filter_pay_type == '-1' && filter_transaction_status == -1 && index >= start_time && index <= end_time && is_admin != -1)
                continue;

            // hide status order without admin
            console.log(data[i].status, data[i].id);
            if(!hide && is_admin == -1){
                if(data[i].status == 5 || data[i].status == 6 || data[i].status == 7){
                    console.log(data[i].status);
                    console.log("hide");
                    hide = true;
                }
            }

            if(index < start_time || index > end_time)
                hide = true;

            if(filter_text != '-1') {
                if (data[i].id.toString().indexOf(filter_text) < 0 && data[i].name.toLowerCase().indexOf(filter_text) < 0 && data[i].email.toLowerCase().indexOf(filter_text) < 0 && data[i].phone.indexOf(filter_text) < 0) {
                    hide = true;
                }
            }

            if(!hide && filter_pay_type != '-1') {
                console.log(filter_pay_type);
                console.log(filter_pay_type.split(","));
                if(filter_pay_type.split(",").indexOf(data[i].pay_type.toString()) === -1){
                    hide = true;
                }
                // if(filter_pay_type != data[i].pay_type) {
                //     hide = true;
                // }
            }

            if(!hide && filter_transaction_status != '-1') {
                if(filter_transaction_status != data[i].status)
                    hide = true;
            }

            if(!hide && filter_ticket_type != '-1') {
                var val = parseInt(filter_ticket_type);
                var found = false;
                for(var j = 0 ; j < data[i].ticket_info.length; j++){
                    if(data[i].ticket_info[j].id_ticket_type == val) {
                        found = true;
                        break;
                    }
                }

                if(!found)
                    hide = true;
            }

            if(hide == true) {
                _ele.hide();
                if(data[i].status > 4)
                    continue;

                current_transaction--;
                current_rev -= data[i].revenue_after;

                for (var j = 0; j < data[i].ticket_info.length; j++) {
                    ticket_types[data[i].ticket_info[j].id_ticket_type]._current_ticket -= data[i].ticket_info[j].number;
                    current_ticket -= data[i].ticket_info[j].number;

                }

                if(data[i].pay_type == 4){
                    var dist = _ele.data('dist');
                    current_shipment -= districts[dist];
                }
            }
        }
        setData();
    }

    $('#doFilter').on('click', function () {
        doFilter();
    });

    $('#filter_text').on('keyup', function (e) {
        filter_text = (e.target.value + "").toLowerCase();
        if ( e.keyCode == 13 ) {
            doFilter();
        }
    });

    $('#filter_ticket_type').on('change', function (e) {
        filter_ticket_type = (e.target.value + "").toLowerCase();
        //doFilter();
    });

    $('#filter_pay_type').on('change', function (e) {
        filter_pay_type = (e.target.value + "").toLowerCase();
        //doFilter();
    });

    $('#filter_transaction_status').on('change', function (e) {
        filter_transaction_status = (e.target.value + "").toLowerCase();
        //doFilter();
    });

    $('#export_data').on('click', function () {
        var data = {
            is_admin: parseInt($('#is_admin').data('data')),
            event_id: $('#event_id').data('data'),
            start_date: new Date($("#startTime").data('date')).getTime(),
            end_date: new Date($("#endTime").data('date')).getTime(),
            ticket_type: filter_ticket_type,
            pay_type: filter_pay_type,
            status: filter_transaction_status
        };

        window.location.href = '/transaction/exportReport?' + $.param(data);
    })
});

checkLinkEventRefresh();
function checkLinkEventRefresh() {
    var event_id_current = window.location.pathname.split('/');
    event_id_current = event_id_current[event_id_current.length - 1];
    if(parseInt(event_id_current) != $('#event_id').data('data')){
        location.href = './' + $('#event_id').data('data');
    }
}
