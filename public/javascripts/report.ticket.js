$( document ).ready(function() {
    var map_pay_type = ["Thanh toán tại quầy", "Thanh toán tại quầy", "Chuyển khoản", "Visa/Master", "COD", "Thanh toán tại quầy", "Mua vé trực tiếp", "ATM", "Thanh Toán MoMo"],
        map_transation_status = ["Đã đặt chỗ", "Đã xác nhận", "Đã đóng gói", "Đang vận chuyển", "Thành công", "Thanh toán thất bại", "Hủy đơn hàng", "Đã hoàn tiền", "Đang xử lý"],
        map_transaction_status_style = ["green_report", "green_report", "green_report", "green_report", "", "red_report", "red_report", "red_report"];
    function setTime(start_item, end_item) {
        $(start_item).datetimepicker({
            format: 'MM/DD/YYYY',
            widgetPositioning: {
                vertical : 'top'
            }
        });
        $(end_item).datetimepicker({
            useCurrent: false, //Important! See issue #1075
            format: 'MM/DD/YYYY',
            widgetPositioning: {
                vertical : 'top'
            }
        });

        $(start_item).on("dp.change", function (e) {
            //$(end_item).data("DateTimePicker").minDate(e.date);
            //$(start_item).data("DateTimePicker").hide();
            //$(end_item).data("DateTimePicker").show();
        });

        $(end_item).on("dp.change", function (e) {
            //$(end_item).data("DateTimePicker").hide();
        });
    }

    setTime('#startTime', '#endTime');
    $('#filter_event').on('change', function (e) {
        console.log(e.target.value);
        location.href = './' + e.target.value;
    });

    var ticket_types = $('#json_ticket_types').data('data');
    var transactions = $('#json_transactions').data('data');
    var summary_data = [], data = [], temp_ticket_types = [];

    summary_data['total'] = [];
    summary_data['total']['paid'] = 0;
    summary_data['total']['processing'] = 0;
    summary_data['current_total'] = [];
    summary_data['current_total']['paid'] = 0;
    summary_data['current_total']['processing'] = 0;

    for(var i = 0 ; i < ticket_types.length; i++){
        summary_data[ticket_types[i].id] = [];
        summary_data[ticket_types[i].id]['paid'] = 0;
        summary_data[ticket_types[i].id]['processing'] = 0;
        temp_ticket_types[ticket_types[i].id] = ticket_types[i];
    }

    var min_time = 9999999999999, max_time = -1;
    for(var i = 0 ; i < transactions.length; i++){
        //if(transactions[i].status > 4)
            //continue;

        if(transactions[i].time_create > max_time) max_time = transactions[i].time_create;
        if(transactions[i].time_create < min_time) min_time = transactions[i].time_create;

        var index = moment(transactions[i].time_create).startOf('day').unix() * 1000;
        if(!data[index]) {
            data[index] = [];
            for(var j = 0 ; j < ticket_types.length; j++){
                data[index][ticket_types[j].id] = [];
                data[index][ticket_types[j].id]['paid'] = 0;
                data[index][ticket_types[j].id]['processing'] = 0;
            }
        }

        transactions[i].ticket_info = JSON.parse(transactions[i].ticket_info);
        for(var j = 0 ; j < transactions[i].ticket_info.length; j++){
            var ticket_info = transactions[i].ticket_info[j];
            if(transactions[i].status == 4) {//succeed
                data[index][ticket_info.id_ticket_type]['paid'] += ticket_info.number;
                summary_data[ticket_info.id_ticket_type]['paid'] += ticket_info.number;
                summary_data['total']['paid'] += ticket_info.number;
            } else {
                if(transactions[i].status == 0){
                    data[index][ticket_info.id_ticket_type]['processing'] += ticket_info.number;
                    summary_data[ticket_info.id_ticket_type]['processing'] += ticket_info.number;
                    summary_data['total']['processing'] += ticket_info.number;
                }
            }
        }
    }

    $("#startTime").data("DateTimePicker").defaultDate(new Date(min_time));
    $("#endTime").data("DateTimePicker").defaultDate(new Date(max_time));

    function setData() {
        var data_string = '';
        for(var i = 0 ; i < ticket_types.length; i++){
            var total_sold = summary_data[ticket_types[i].id]['paid'] + summary_data[ticket_types[i].id]['processing'];
            var percent_1 = (total_sold * 100 / ticket_types[i].total).toFixed(2);
            var percent_2 = (summary_data[ticket_types[i].id]['paid'] * 100 / total_sold).toFixed(2);
            data_string += '<div class="table_report_one" style="height: 100%;padding-top:20px;">' +
                                '<div class="col-sm-8" style="height: 100%">' +
                                    '<p><strong>' + ticket_types[i].name_vi + '</strong></p>' +
                                '</div>' +
                                '<div class="col-sm-4" style="height: 100%">' +
                                    '<p>Đã đặt chỗ <strong style="color:#2fb858">' + total_sold + '</strong><strong style="color: black"> / ' + ticket_types[i].total + '</strong> (' + percent_1 + '%)</p>' +
                                    '<p>Đã thanh toán <strong>' + summary_data[ticket_types[i].id]['paid'] + '</strong><strong style="color:black"> / </strong><strong style="color:#2fb858">' + total_sold + '</strong> (' + percent_2 + '%)</p>' +
                                    /*'<p>Mở bán đến hết ngày <strong>' + ticket_types[i].end_date + '</strong></p>' +*/
                                '</div>' +
                                '<div class="chart_report col-sm-12" style="height: 100%">' +
                                    '<div class="back_chart" style="width: ' + percent_1 + '%;">' +
                                        '<div class="back_chart" style="width: ' + percent_2 + '%;background-color:#358dd1"></div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
        }
        $('#ticket_summary').append(data_string);
    }

    function doFilter() {
        var start_time = new Date($("#startTime").data('date')).getTime();
        var end_time = new Date($("#endTime").data('date')).getTime();
        var data_string = '';
        var total = [];
        for(var i = 0 ; i < ticket_types.length * 2; i++) {
            total[i] = 0;
        }
        for(var d in data){
            if(d >= start_time && d <= end_time) {
                data_string += '<tr><td>' + moment(parseInt(d)).format("MM/DD/YYYY") + '</td>';
                for (var j = 0; j < ticket_types.length; j++) {
                    data_string += '<td style="text-align: right">' + data[d][ticket_types[j].id]['processing'].toLocaleString() + '</td><td style="text-align: right">' + data[d][ticket_types[j].id]['paid'].toLocaleString() + '</td>';
                    total[j * 2] += data[d][ticket_types[j].id]['processing'];
                    total[j * 2 + 1] += data[d][ticket_types[j].id]['paid'];
                }
                data_string += '</tr>'
            }
        }

        data_string += '<tr><td>TOTAL</td>';
        for(var i = 0 ; i < total.length; i++){
            data_string += '<td style="text-align: right">' + total[i].toLocaleString() + '</td>';
        }
        data_string += '</tr>';
        $('#ticket_detail tbody').empty();
        $('#ticket_detail tbody').append(data_string);
    }

    setData();
    doFilter();

    $('#doFilter').on('click', function () {
        doFilter();
    });

    $('#filter_text').on('keyup', function (e) {
        filter_text = (e.target.value + "").toLowerCase();
        //doFilter();
    });

    $('#filter_ticket_type').on('change', function (e) {
        filter_ticket_type = (e.target.value + "").toLowerCase();
        //doFilter();
    });

    $('#filter_pay_type').on('change', function (e) {
        filter_pay_type = (e.target.value + "").toLowerCase();
        //doFilter();
    });

    $('#filter_transaction_status').on('change', function (e) {
        filter_transaction_status = (e.target.value + "").toLowerCase();
        //doFilter();
    });
});
