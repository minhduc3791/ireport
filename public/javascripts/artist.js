$( document ).ready(function() {
    $('#table_artist').dataTable({
        'sDom': 't',
        "paging": false,
        "sorting": [[0, "asc"], [1, "asc"], [2, "asc"]]
    });

    var table = $('#table_artist').DataTable();
    $('#filter_artist').on( 'keyup', function () {
        table
            .columns( 2 )
            .search( this.value )
            .draw();
    } );
});