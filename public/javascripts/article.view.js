$( document ).ready(function() {
    $('#table_article').dataTable({
        'sDom': 't',
        "paging": false,
        "sorting": [[0, "asc"], [1, "asc"], [2, "asc"]],
        searchHighlight: true
    });

    var table = $('#table_article').DataTable();
    $('#filter_article').on( 'keyup', function () {
        table
            .columns( 4 )
            .search( this.value )
            .draw();
    } );
});