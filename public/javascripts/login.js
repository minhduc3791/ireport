$( document ).ready(function() {
    var url = $('#url').data('data');
    $('#password').on('keypress', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13)
            $('#doLogin').click();
    });

    $('#doLogin').on('click', function () {
        var data = {
            username: $('#username').val(),
            password : $('#password').val()
        };

        $.ajax({
            type: "POST",
            url: "/doLogin",
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(data){
                if(data == "success"){
                    location.reload();
                }
            },
            failure: function(data) {
                console.log(2, data);
            }
        });
    })
});