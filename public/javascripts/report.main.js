$( document ).ready(function() {
    $( ".icon_user i" ).click(function() {
        $( ".user_main" ).toggle( "slow", function() {
            // Animation complete.
        });
    });

    $('#logout').on('click', function () {
        $.ajax({
            type: "GET",
            url: "/doLogout",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(data){
                if(data.data == "success"){
                    location.reload();
                }
            },
            failure: function(data) {
                console.log(2, data);
            }
        });
    });
});