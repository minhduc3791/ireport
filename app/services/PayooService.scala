package services

import java.io.ByteArrayOutputStream
import java.net.{URLDecoder, URLEncoder}
import javax.xml.bind.JAXBContext
import javax.xml.transform.stream.StreamResult

import controllers.ResponsePayoo
import dao.{TransactionDAO, TransactionDetailDAO, TransactionSuccessDAO}
import helpers.{Common, JsonExt}
import models.{CustomerPayoo, OrderPayoo, TicketInfo}
import play.api.db.Database
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.mvc.ControllerComponents

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

import scala.concurrent.ExecutionContext.Implicits.global

class PayooService (ws: WSClient, db: Database, val controllerComponents: ControllerComponents) {
    private val _SecretKey = "e2c76845e3abc589d79eca0533ad18e4"
    private val ShopName = "DreamsPass"
    private val ShopId = 1220L
    private val ShopTitle = "DreamsPass"
    private val APIUsername = "DreamsPass_BizAPI"
    private val APIPassword = "dt0UwccgjFdoN9CN"
    private val APISignature = "k7iqdsx5jmOJ+SJX2qbrcuv/dd5qkfEvn0QNj2ToDDNiP3NEhwZdKGtLHRNZ2J02"
    private val transactionSuccessDAO: TransactionSuccessDAO = new TransactionSuccessDAO(db, controllerComponents)
    private val transactionDetailDAO: TransactionDetailDAO = new TransactionDetailDAO(db, controllerComponents)
    private val transactionDAO: TransactionDAO = new TransactionDAO(db, controllerComponents)

    def createOrder(method: String, order_no: String, amount: String, username: String, phonenumber: String, address: String, email: String): String = {
        val domain = "http://www.dreamspass.vn"
        //val domain = "http://localhost"
        val shopBackUrl = "https://dreamspass.tech:9112/payoo/callback"
        //val shopBackUrl = "http://localhost:9000/payoo/callback"
        val notifyUrl = "https://dreamspass.vn/payoonotify.php"
        //val notifyUrl = "http://localhost"

        val now = System.currentTimeMillis() + 7 * 60 * 60 * 1000 // adjust gmt
        val ship_date = now + 2 * 24 * 60 * 60 * 1000 // 2 days
        val ship_days = 2
        val valid_time = now + 10 * 60 * 1000

        val transactionDetail = transactionDetailDAO.getTransactionDetailById(order_no.toLong)
        val ticket_infos = JsonExt.gson.fromJson(transactionDetail.ticket_info, classOf[Array[TicketInfo]])
        var order_des = "Order No: " + order_no + ".</br>" +
            transactionDetail.event_name_en + "</br>" +
            ticket_infos.foldLeft("")((a, ticket_info) => a + ticket_info.name_en_ticket_type + ": " + ticket_info.number + "</br>") +
            "</br>Money total: " + amount

        order_des = URLEncoder.encode(order_des, "UTF-8")

        val customer_payoo = new CustomerPayoo(username, phonenumber, address, email)
        val order_payoo = new OrderPayoo(ShopName, ShopId, order_no, ShopTitle, domain, URLEncoder.encode(shopBackUrl, "UTF-8"),
            order_no, amount, Common.convertTimestamp(ship_date, "dd/MM/yyyy"), ship_days, order_des, URLEncoder.encode(notifyUrl, "UTF-8"),
            Common.convertTimestamp(valid_time), customer_payoo)
        val res: StreamResult = new StreamResult(new ByteArrayOutputStream())
        JAXBContext.newInstance(classOf[OrderPayoo]).createMarshaller.marshal(order_payoo, res)
        var data = res.getOutputStream.toString()
        data = "<shops>" + data.substring(55, data.length) + "</shops>"
        val checksum = URLEncoder.encode(Common.generate("SHA-512", _SecretKey + data), "UTF-8")
        data = URLEncoder.encode(data, "UTF-8")
        val refer = URLEncoder.encode(domain, "UTF-8")
        val restrict = URLEncoder.encode("1", "UTF-8")
        val bank = ""
        val url = "https://www.payoo.vn/v2/paynow/order/create"
        val post_data = "data=" + data + "&checksum=" + checksum + "&refer=" + refer + "&restrict=" + restrict + "&method=" + URLEncoder.encode(method, "UTF-8") + "&bank=" + bank

        val futureResult: Future[WSResponse] = ws.url(url).addHttpHeaders("Content-Type" -> "application/x-www-form-urlencoded").post(post_data).map { r => r }
        val result = Await.result(futureResult, 10 second)
        val response = JsonExt.gson.fromJson(result.body, classOf[ResponsePayoo])

        response.order.payment_url
    }

    def mapFieldData(field: String, node: scala.xml.NodeSeq): String = {
        field match {
            case "PaymentMethod" => Common.getXMLValue(node, "PaymentMethod")
            case "State" => Common.getXMLValue(node, "State")
            case "Session" => Common.getXMLValue(node, "session")
            case "BusinessUsername" => Common.getXMLValue(node, "username")
            case "ShopID" => Common.getXMLValue(node, "shop_id")
            case "ShopTitle" => Common.getXMLValue(node, "shop_title")
            case "ShopDomain" => Common.getXMLValue(node, "shop_domain")
            case "ShopBackUrl" => Common.getXMLValue(node, "shop_back_url")
            case "OrderNo" => Common.getXMLValue(node, "order_no")
            case "OrderCashAmount" => Common.getXMLValue(node, "order_cash_amount")
            case "StartShippingDate" => Common.getXMLValue(node, "order_ship_date")
            case "ShippingDays" => Common.getXMLValue(node, "order_ship_days")
            case "OrderDescription" => URLDecoder.decode(Common.getXMLValue(node, "order_description"), "UTF-8")
            case "NotifyUrl" => Common.getXMLValue(node, "notify_url")
            case "PaymentExpireDate" => Common.getXMLValue(node, "validity_time")
            case _ => ""
        }
    }
}
