package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.{Seat, Transaction}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class SeatDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parseSeat = {
        get[Long]("seat_id") ~
        get[Long]("event_id") ~
            get[Long]("seat_map_id") ~
            get[String]("seat_name_en") ~
            get[String]("seat_name_vi") ~
            get[Long]("ticket_type_id") ~
            get[Int]("is_enable") ~
            get[Int]("is_locked") ~
            get[Int]("is_couple") ~
            get[String]("seat_note_en") ~
            get[String]("seat_note_vi") ~
            get[Int]("is_sold") ~
            get[Long]("locked_time") ~
            get[String]("related_seats") map {
            case seat_id ~ event_id ~ seat_map_id ~ seat_name_en ~ seat_name_vi ~ ticket_type_id ~ is_enable ~ is_locked ~ is_couple ~ seat_note_en ~ seat_note_vi ~ is_sold ~ locked_time ~ related_seats
            => Seat(seat_id, event_id, seat_map_id, seat_name_en, seat_name_vi, ticket_type_id, is_enable, is_locked, is_couple, seat_note_en, seat_note_vi, is_sold, locked_time, related_seats)
        }
    }

    def getSeatsBySeatMapId(seat_map_id: Long, event_id: Long): List[Seat] = {
        db.withConnection { implicit connection =>
            val seats = SQL("""SELECT * FROM seat WHERE seat_map_id={seat_map_id} AND event_id={event_id}""")
                .on("seat_map_id" -> seat_map_id).on("event_id" -> event_id)
                .as(parseSeat.*)

            seats
        }
    }

    def getSeat(seat_id: Long, seat_map_id: Long , event_id: Long): Seat = {
        db.withConnection { implicit connection =>
            val parser: RowParser[Seat] = Macro.namedParser[Seat]
            val seat = SQL("SELECT * FROM seat WHERE seat_id={seat_id} AND seat_map_id={seat_map_id} AND event_id={event_id}")
                .on("seat_id" -> seat_id).on("seat_map_id" -> seat_map_id).on("event_id" -> event_id)
                .as(parseSeat.*)

            seat(0)
        }
    }

    def buySeat(seat_id: Long) : Int = {
        db.withConnection { implicit connection =>
            SQL("UPDATE seat SET is_sold=1 WHERE seat_id={seat_id}")
                .on("seat_id" -> seat_id)
                .executeUpdate()
        }
    }

    def unbuySeat(seat_id: Long) : Int = {
        db.withConnection { implicit connection =>
            SQL("UPDATE seat SET is_sold=0, is_locked=0 WHERE seat_id={seat_id}")
                .on("seat_id" -> seat_id)
                .executeUpdate()
        }
    }

    def lockSeat(seat_id: Long, locked_time: Long) : Int = {
        db.withConnection { implicit connection =>
            SQL("UPDATE seat SET is_locked=1, locked_time={locked_time} WHERE seat_id={seat_id}")
                .on("seat_id" -> seat_id).on("locked_time" -> locked_time)
                .executeUpdate()
        }
    }

    def unlockSeat(seat_id: Long) : Int = {
        db.withConnection { implicit connection =>
            SQL("UPDATE seat SET is_locked=0 WHERE seat_id={seat_id}")
                .on("seat_id" -> seat_id)
                .executeUpdate()
        }
    }

    def updateSeataInfo(seat: Seat) : Int = {
        db.withConnection { implicit connection =>
            SQL("UPDATE seat SET seat_name_en={seat_name_en}, seat_name_vi={seat_name_vi}, ticket_type_id={ticket_type_id}, seat_note_en={seat_note_en}, seat_note_vi={seat_note_vi} WHERE seat_id={seat_id}")
                .on("seat_name_en" -> seat.seat_name_en).on("seat_name_vi" -> seat.seat_name_vi).on("ticket_type_id" -> seat.ticket_type_id).on("seat_note_en" -> seat.seat_note_en)
                .on("seat_note_vi" -> seat.seat_note_vi).on("seat_id" -> seat.seat_id)
                .executeUpdate()
        }
    }
}
