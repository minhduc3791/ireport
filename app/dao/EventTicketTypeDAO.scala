package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.{EventTicketType}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

case class Verify(var total_ticket_sold: Int, var total: Int)
class EventTicketTypeDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parsedTicket = {
        get[Long]("id") ~
            get[Long]("event_id") ~
            get[String]("name_vi") ~
            get[String]("name_en") ~
            get[Int]("total") ~
            get[Long]("price") ~
            get[Int]("min_buy") ~
            get[Int]("max_buy") ~
            get[Long]("start") ~
            get[Long]("end") ~
            get[String]("start_date") ~
            get[String]("end_date") ~
            get[String]("start_time") ~
            get[String]("end_time") ~
            get[String]("type_ticket") ~
            get[String]("description_vi") ~
            get[String]("description_en") ~
            get[String]("note") ~
            get[Int]("stop_ticketing") ~
            get[Int]("only_payment_office") ~
            get[String]("schedule_text") ~
            get[Int]("enable") ~
            get[Int]("total_ticket_sold") map {
            case id ~ event_id ~ name_vi ~ name_en ~ total ~ price ~ min_buy ~ max_buy ~ start ~ end ~ start_date ~ end_date ~ start_time ~ end_time ~ type_ticket ~ description_vi ~ description_en
                ~ note ~ stop_ticketing ~ only_payment_office ~ schedule_text ~ enable ~total_ticket_sold
            => EventTicketType(id, event_id, name_vi, name_en, total, price, min_buy, max_buy, start, end, start_date, end_date, start_time, end_time, type_ticket, description_vi, description_en
                , note, stop_ticketing, only_payment_office, schedule_text, enable, total_ticket_sold)
        }
    }

    val parseVerify = {
        get[Int]("total_ticket_sold") ~
            get[Int]("total") map {
            case total_ticket_sold ~ total
            => Verify(total_ticket_sold, total)
        }
    }

    def getTicketByEvent(event_id: Long): List[EventTicketType] =  {
        db.withConnection { implicit connection =>
            SQL("""SELECT * FROM event_ticket_type WHERE event_id={event_id}""")
                .on("event_id" -> event_id)
                .as(parsedTicket.*)
        }
    }

    def verify(id: Long) : Verify = {
        db.withConnection { implicit connection =>
            val verify = SQL("SELECT total_ticket_sold, total FROM event_ticket_type where id = {id}")
                .on("id" -> id)
                .as(parseVerify.*)

            verify(0)
        }
    }

    def buyTicket(id: Long, count: Int) = {
        db.withConnection { implicit connection =>
            val verify = SQL("UPDATE event_ticket_type SET total_ticket_sold=total_ticket_sold+{count} WHERE id={id}")
                .on("id" -> id).on("count" -> count)
                .executeUpdate()
        }
    }
}
