package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.{Event, Transaction, TransactionDetail, TransactionSuccess}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class TransactionSuccessDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    //val table_name = "transaction"
    val table_name = "event_order"
    val parsedTransactionSuccess = {
        get[String]("banner") ~
            get[String]("email") ~
            get[String]("name_vi") ~
            get[String]("name_en") ~
            get[String]("venue") ~
            get[String]("address") ~
            get[Long]("order_id") ~
            get[Long]("time_create") ~
            get[String]("ticket_info") ~
            get[Int]("revenue_after") ~
            get[Int]("revenue_before") ~
            get[Int]("revenue_discount") ~
            get[Int]("pay_type") map {
            case banner ~ email ~ name_vi ~ name_en ~ venue ~ address ~ order_id ~ time_create ~ ticket_info ~ revenue_after ~ revenue_before ~ revenue_discount ~ pay_type
            => TransactionSuccess(banner, email, name_vi, name_en, venue, address, order_id, time_create, ticket_info, revenue_after, revenue_before, revenue_discount, pay_type)
        }
    }

    def getTransactionSuccessById(id: Long): TransactionSuccess = {
        db.withConnection { implicit connection =>
            val parser: RowParser[TransactionSuccess] = Macro.namedParser[TransactionSuccess]
            val trans = SQL("SELECT t.*, e.* FROM " + table_name + " t JOIN event e ON t.event_id=e.event_id WHERE id={id}")
                .on("id" -> id)
                .as(parsedTransactionSuccess.*)

            trans(0)
        }
    }
}
