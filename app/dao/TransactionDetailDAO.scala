package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.{Event, Transaction, TransactionDetail}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class TransactionDetailDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    //val table_name = "transaction"
    val table_name = "event_order"
    val parsedTransactionDetail = {
        get[Long]("id") ~
            get[Int]("pay_type") ~
            get[Long]("time_create") ~
            get[Long]("time_payment_success") ~
            get[Int]("status") ~
            get[String]("name") ~
            get[String]("email") ~
            get[String]("phone") ~
            get[Option[String]]("city_name") ~
            get[Option[String]]("district") ~
            get[Option[String]]("ward") ~
            get[String]("ticket_info") ~
            get[Long]("revenue_before") ~
            get[Long]("revenue_discount") ~
            get[Long]("revenue_after") ~
            get[Option[String]]("apply_voucher") ~
            get[Long]("event_id") ~
            get[String]("event_name_vi") ~
            get[String]("event_name_en") ~
            get[String]("note") ~
        get[String]("transaction_address") ~
        get[String]("event_address") map {
            case id ~ pay_type ~ time_create ~ time_payment_success ~ status ~ name ~ email ~ phone ~ city_name ~ district ~ ward ~ ticket_info
                ~ revenue_before ~ revenue_discount ~ revenue_after ~ apply_voucher ~ event_id ~ event_name_vi ~ event_name_en ~ note ~ transaction_address ~ event_address
            => TransactionDetail(id, pay_type, time_create, time_payment_success, status, name, email, phone, city_name, district, ward, ticket_info,
                revenue_before, revenue_discount, revenue_after, apply_voucher, event_id, event_name_vi, event_name_en, note, transaction_address, event_address)
        }
    }

    def getTransactionDetailById(id: Long): TransactionDetail = {
        db.withConnection { implicit connection =>
            val parser: RowParser[TransactionDetail] = Macro.namedParser[TransactionDetail]
            val trans = SQL("SELECT t.address as transaction_address, t.id, t.invoice_vat, t.pay_type, t.time_create, t.time_payment_success, t.status, t.name, t.email, t.phone, c.name as city_name, d.id as district, w.id as ward, t.ticket_info, t.revenue_before, t.revenue_discount, t.revenue_after, t.apply_voucher, t.event_id, t.note, e.address as event_address, e.name_vi as event_name_vi, e.name_en as event_name_en FROM " + table_name + " t JOIN event e ON t.event_id=e.id LEFT JOIN city c ON t.city=c.id" +
                " LEFT JOIN district d ON t.district=d.id LEFT JOIN ward w ON t.ward=w.id WHERE t.id={id}")
                .on("id" -> id)
                .as(parsedTransactionDetail.*)

            if(trans.length > 0)
                trans(0)
            else
                null
        }
    }

    def getTransactionDetailByEventId(id: Long): List[TransactionDetail] = {
        db.withConnection { implicit connection =>
            val parser: RowParser[TransactionDetail] = Macro.namedParser[TransactionDetail]
            val trans = SQL("SELECT t.address as transaction_address, t.id, t.invoice_vat, t.pay_type, t.time_create, t.time_payment_success, t.status, t.name, t.email, t.phone, c.name as city_name, d.id as district, w.id as ward, t.ticket_info, t.revenue_before, t.revenue_discount, t.revenue_after, t.apply_voucher, t.event_id, t.note, e.address as event_address, e.name_vi as event_name_vi, e.name_en as event_name_en FROM " + table_name + " t JOIN event e ON t.event_id=e.id LEFT JOIN city c ON t.city=c.id" +
                " LEFT JOIN district d ON t.district=d.id LEFT JOIN ward w ON t.ward=w.id WHERE t.event_id={id}")
                .on("id" -> id)
                .as(parsedTransactionDetail.*)

            trans
        }
    }
}
