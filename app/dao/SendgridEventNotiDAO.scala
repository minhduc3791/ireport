package dao

import org.joda.time.{DateTime, DateTimeZone}
import anorm.SqlParser._
import anorm._
import helpers.Common
import javax.inject.Inject
import models.SendgridEventNoti
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class SendgridEventNotiDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parseSendgridEventNoti = {
            get[Option[Long]]("id") ~
            get[Option[Long]]("order_id") ~
            get[Option[Long]]("event_id") ~
            get[Option[Int]]("order_status") ~
            get[Option[Int]]("email_status") ~
            get[Option[DateTime]]("email_time") map {
                case id ~ order_id ~ event_id ~ order_status ~ email_status ~ email_time
            => SendgridEventNoti(id.getOrElse(0), order_id.getOrElse(0), event_id.getOrElse(0), order_status.getOrElse(0), email_status.getOrElse(0), email_time.getOrElse(new DateTime()))
        }
    }

//    def insertSendgridEventNoti(sendgridEventNoti: SendgridEventNoti): Long= {
//        val queryString = Common.buildInsertQuery(sendgridEventNoti.getClass)
//        var result: Long = -1
//        db.withConnection { implicit connection =>
//            val id: Option[Long] = SQL(queryString).on("email" -> sendgridEventNoti.email).on("timestamp" -> sendgridEventNoti.timestamp)
//                  .on("event" -> Option(sendgridEventNoti.event).getOrElse("")).on("smtp_id" -> sendgridEventNoti.smtp_id).on("useragent" -> sendgridEventNoti.useragent)
//                  .on("ip" -> sendgridEventNoti.ip).on("sg_event_id" -> sendgridEventNoti.sg_event_id).on("sg_message_id" -> sendgridEventNoti.sg_message_id)
//                  .on("reason" -> sendgridEventNoti.reason).on("status" -> sendgridEventNoti.status).on("response" -> sendgridEventNoti.response)
//                  .on("tls" -> sendgridEventNoti.tls).on("url" -> sendgridEventNoti.url).on("attempt" -> sendgridEventNoti.attempt)
//                  .on("category" -> sendgridEventNoti.category.mkString(",")).on("bounce_type" -> sendgridEventNoti.bounce_type)
//                .executeInsert()
//
//            result = id match {
//                case Some(number) => number
//                case None => -1
//            }
//        }
//
//        result
//    }

    def getSendgridEventNoti(transaction_id: Long): List[SendgridEventNoti] = {
        db.withConnection { implicit connection =>
            val datas = SQL("SELECT * FROM email_history WHERE order_id = '" + transaction_id + "'")
              .as(parseSendgridEventNoti.*)

            datas
        }
    }
}
