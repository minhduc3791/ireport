package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.{Event, ListEvent}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}


class EventDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parsedEvent = {
        get[Long]("id") ~
            get[String]("link") ~
            get[String]("name_vi") ~
            get[String]("name_en") ~
            get[String]("event_type") ~
            get[String]("position") ~
            get[String]("address") ~
            get[String]("location") ~
            get[String]("venue") ~
            get[String]("district") ~
            get[String]("city") ~
            get[String]("banner") ~
            get[String]("poster") ~
            get[String]("sit_map") ~
            get[Boolean]("enable") ~
            get[Long]("date_show") ~
            get[String]("description_vi") ~
            get[String]("description_en") ~
            get[String]("email_contact") ~
            get[String]("phone_contact") ~
            get[String]("list_organizer") ~
            get[String]("list_condition") ~
            get[Int]("time_remind_payment") ~
            get[Int]("time_cancel_payment") map {
            case id ~ link ~ name_vi ~ name_en ~ event_type ~ position ~ address ~ location ~ venue ~ district ~ city ~ banner ~ poster ~ sit_map ~ enable ~ date_show ~
                description_vi ~ description_en ~ email_contact ~ phone_contact ~ list_organizer ~ list_condition ~ time_remind_payment ~ time_cancel_payment
            => Event(id, link, name_vi, name_en, event_type, position, address, location, venue, district, city, banner, poster, sit_map, enable, date_show,
                description_vi, description_en, email_contact, phone_contact, list_organizer, list_condition, time_remind_payment, time_cancel_payment)
        }
    }

    val parsedListEvent = {
        get[Long]("id") ~
        get[String]("name_vi") ~
        get[String]("name_en") map {
            case id ~ name_vi ~ name_en
                => ListEvent(id, name_vi, name_en)
        }
    }

    def getEventByEventId(event_id: Long): Event = {
        db.withConnection { implicit connection =>
            val events = SQL("""SELECT * FROM event WHERE id={event_id}""")
                .on("event_id" -> event_id)
                .as(parsedEvent.*)

            events(0)
        }
    }

    def getListEvent(): List[ListEvent] = {
        db.withConnection { implicit connection =>
            val events = SQL("""SELECT id, name_vi, name_en FROM event WHERE enable=1 ORDER BY id DESC""")
                .as(parsedListEvent.*)

            events
        }
    }

    def getListEventByUser(user_id: Long): List[ListEvent] = {
        db.withConnection { implicit connection =>
            val events = SQL("""SELECT event.id, event.name_vi, event.name_en FROM event INNER JOIN permission ON permission.event_id = event.id WHERE event.enable=1 AND permission.user_id = {user_id} GROUP BY event.id ORDER BY event.id DESC""")
              .on("user_id" -> user_id)
              .as(parsedListEvent.*)

            events
        }
    }
}
