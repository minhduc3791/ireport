package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.District
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class DistrictDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parseDistrict = {
        get[String]("id") ~
            get[String]("name") ~
            get[String]("type_district") ~
            get[String]("city_id") ~
            get[Int]("shipment_price") map {
            case id ~ name ~ type_district ~ city_id ~ shipment_price
            => District(id, name, type_district, city_id, shipment_price)
        }
    }

    def getDistrictByCityId(city_id: String): List[District] = {
        db.withConnection { implicit connection =>
            val districts = SQL("""SELECT * FROM district WHERE city_id={city_id}""")
                .on("city_id" -> city_id)
                .as(parseDistrict.*)

            districts
        }
    }
}
