package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.{User}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class UserDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parseUser = {
        get[Long]("id") ~
            get[String]("phone") ~
            get[String]("full_name") ~
            get[String]("user_name") ~
            get[String]("email") ~
            get[String]("password") ~
            get[String]("gender") ~
            get[String]("dob") ~
            get[String]("organization") ~
            get[Long]("time_create") ~
            get[String]("note") ~
            get[Boolean]("enable") ~
            get[Long]("permission_group_id") map {
            case id ~ phone ~ full_name ~ user_name ~ email ~ password ~ gender ~ dob ~ organization ~ time_create ~ note ~ enable ~ permission_group_id
            => User(id, phone, full_name, user_name, email, password, gender, dob, organization, time_create, note, enable, permission_group_id)
        }
    }

    def verifyUserName(user_name: String): User = {
        db.withConnection { implicit connection =>
            val users = SQL("""SELECT * FROM user WHERE user_name={user_name} AND enable = 1""")
                .on("user_name" -> user_name)
                .as(parseUser.*)

            if(users.length > 0)
                users(0)
            else
                null
        }
    }
}
