package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import helpers.JsonExt
import models.{TicketInfo, Transaction}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class TransactionDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    //val table_name = "transaction"
    val table_name = "event_order"
    val parsedTransaction = {
        get[Long]("id") ~
            get[Long]("event_id") ~
            get[Int]("gender") ~
            get[String]("name") ~
            get[String]("phone") ~
            get[String]("email") ~
            get[String]("company_name") ~
            get[String]("company_address") ~
            get[String]("company_tax_code") ~
            get[String]("city") ~
            get[String]("district") ~
            get[String]("ward") ~
            get[String]("address") ~
            get[Long]("user") ~
            get[String]("note") ~
            get[Int]("pay_type") ~
            get[String]("apply_voucher") ~
            get[Int]("revenue_before") ~
            get[Int]("revenue_discount") ~
            get[Int]("revenue_after") ~
            get[Long]("time_create") ~
            get[Int]("invoice_vat") ~
            get[Int]("status") ~
            get[String]("ticket_info") ~
            get[Long]("time_payment_success") ~
            get[String]("time_send_email") ~
            get[Int]("language_code") map {
            case id ~ event_id ~ gender ~ name ~ phone ~ email ~ company_name ~ company_address ~ company_tax_code ~ city ~ district ~ ward ~ address ~ user
                ~ note ~ pay_type ~ apply_voucher ~ revenue_before ~ revenue_discount ~ revenue_after ~ time_create ~ invoice_vat ~ status ~ ticket_info ~ time_payment_success ~ time_send_email ~ language_code
            => Transaction(id, event_id, gender, name, phone, email, company_name, company_address, company_tax_code, city, district, ward,
                address, user, note, pay_type, apply_voucher, revenue_before, revenue_discount, revenue_after, time_create, invoice_vat, status, ticket_info, time_payment_success, time_send_email, language_code)
        }
    }

    def insert(transaction: Transaction): Long = {
        var result: Long = -1
        db.withConnection { implicit connection =>
            val id: Option[Long] = SQL("INSERT into " + table_name + "(id, event_id, gender, name, phone, email, company_name, company_address, company_tax_code, city, district, ward, address, user, note, pay_type, apply_voucher, revenue_before, revenue_discount, revenue_after, time_create, invoice_vat, status, ticket_info, time_payment_success, time_send_email, language_code) " +
                "value({id}, {event_id}, {gender}, {name}, {phone}, {email}, {company_name}, {company_address}, {company_tax_code}, {city}, {district}, {ward}, {address}, {user}, {note}, {pay_type}, {apply_voucher}, {revenue_before}, {revenue_discount}, {revenue_after}, {time_create}, {invoice_vat}, {status}, {ticket_info}, {time_payment_success}, {time_send_email}, {language_code})")
                .on("id" -> transaction.id)
                .on("event_id" -> transaction.event_id)
                .on("gender" -> transaction.gender)
                .on("name" -> transaction.name)
                .on("phone" -> transaction.phone)
                .on("email" -> transaction.email)
                .on("company_name" -> transaction.company_name)
                .on("company_address" -> transaction.company_address)
                .on("company_tax_code" -> transaction.company_tax_code)
                .on("city" -> transaction.city).on("district" -> transaction.district).on("ward" -> transaction.ward).on("address" -> transaction.address).on("user" -> transaction.user).on("note" -> transaction.note).on("pay_type" -> transaction.pay_type).on("apply_voucher" -> transaction.apply_voucher).on("revenue_before" -> transaction.revenue_before)
                .on("revenue_discount" -> transaction.revenue_discount).on("revenue_after" -> transaction.revenue_after).on("time_create" -> transaction.time_create).on("invoice_vat" -> transaction.invoice_vat).on("status" -> transaction.status).on("ticket_info" -> transaction.ticket_info).on("time_payment_success" -> transaction.time_payment_success)
                .on("time_send_email" -> transaction.time_send_email)
                .on("language_code" -> transaction.language_code)
                .executeInsert()

            result = id match {
                case Some(number) => number
                case None => -1
            }
        }

        result
    }

    def insertWithTransaction(transaction: Transaction) = {
        db.withTransaction{ implicit connection =>
            insert(transaction)

        }
    }

    def getTransactionById(id: Long) : Transaction = {
        db.withConnection { implicit connection =>
            val parser: RowParser[Transaction] = Macro.namedParser[Transaction]
            val transaction = SQL("SELECT * FROM " + table_name + " where id = {id}")
                .on("id" -> id)
                .as(parser.*)
            transaction(0)
        }
    }

    def getTransactions(offset: Long, limit: Long): List[Transaction] = {
        db.withConnection { implicit connection =>
            val parser: RowParser[Transaction] = Macro.namedParser[Transaction]
            val trans = SQL("SELECT * FROM " + table_name +" LIMIT {limit} OFFSET {offset}")
                .on("limit" -> limit).on("offset" -> offset)
                .as(parsedTransaction.*)

            trans
        }
    }

    def getTransactionsWithFilters(is_admin: Int, event_id: Long, start_date: Long, end_date: Long, pay_type: String, status: Int, ticket_type: Long): List[Transaction] = {
        var queryString = "SELECT * FROM " + table_name + " WHERE event_id={event_id}"

        queryString += " AND time_create>={start_date} AND time_create"
        if(start_date > -1 && end_date > -1) queryString += "<"  else queryString += ">"
        queryString += "={end_date}"

        val pay_types = pay_type.split(',')
        queryString += " AND ( "

        for (i <- 0 until pay_types.length) {
            if(i > 0){
                queryString += " OR "
            }
            queryString += " pay_type"
            if(pay_types(i).toInt > -1) queryString += "=" else queryString += ">"
            queryString += pay_types(i).toInt
        }
        queryString += " ) "

        //        queryString += " AND pay_type"
//        if(pay_type > -1) queryString += "=" else queryString += ">"
//        queryString += "{pay_type}"

        queryString += " AND status"
        if(status > -1) queryString += "=" else queryString += ">"
        queryString += "{status}"

        if(is_admin == -1 && status == -1){
            queryString += " AND status NOT IN (5, 6, 7) "
        }

        queryString += " ORDER BY time_create DESC"

//        println("-----------137---------")
//        println(queryString);
//        println("-----------139---------")

        db.withConnection { implicit connection =>
            var trans = SQL(queryString)
                .on("event_id" -> event_id).on("start_date" -> start_date).on("end_date" -> end_date).on("status" -> status)
                .as(parsedTransaction.*)

//            var trans = SQL(queryString)
//              .on("event_id" -> event_id).on("start_date" -> start_date).on("end_date" -> end_date).on("pay_type" -> pay_type).on("status" -> status)
//              .as(parsedTransaction.*)

            if(ticket_type > -1)
                trans = trans.filter{t =>
                    JsonExt.gson.fromJson(t.ticket_info, classOf[Array[TicketInfo]]).filter(f => f.id_ticket_type == ticket_type).length > 0
                }
            trans
        }
    }

    def getTransactionsByDate(start_date: Long, end_date: Long): List[Transaction] = {
        db.withConnection { implicit connection =>
            val trans = SQL("SELECT * FROM " + table_name + " WHERE time_create>={start_date} AND time_create<={end_date}")
                .on("start_date" -> start_date).on("end_date" -> end_date)
                .as(parsedTransaction.*)

            trans
        }
    }

    def getTransactionsByEvent(event_id: Long): List[Transaction] = {
        db.withConnection { implicit connection =>
            val trans = SQL("SELECT * FROM " + table_name + " WHERE event_id={event_id} ORDER BY CASE status WHEN 6 THEN -1 WHEN 5 THEN -1 WHEN 7 THEN -1 ELSE 1 END DESC, time_create DESC")
                .on("event_id" -> event_id)
                .as(parsedTransaction.*)

            trans
        }
    }

    def getTransactionsById(id: Long): Transaction = {
        db.withConnection { implicit connection =>
            val parser: RowParser[Transaction] = Macro.namedParser[Transaction]
            val trans = SQL("SELECT * FROM " + table_name + " WHERE id={id}")
                .on("id" -> id)
                .as(parsedTransaction.*)

            trans(0)
        }
    }

    def updateTransactionNote(id: Long, note: String) : Int = {
        db.withConnection { implicit connection =>
            SQL("UPDATE " + table_name + " SET note={note} WHERE id={id}")
                .on("note" -> note).on("id" -> id)
                .executeUpdate()
        }
    }

    def updateTransactionStatus(id: Long, status: Int) : Int = {
        db.withConnection { implicit connection =>
            SQL("UPDATE " + table_name + " SET status={status} WHERE id={id}")
                .on("status" -> status).on("id" -> id)
                .executeUpdate()
        }
    }

    def updateTransactionUserInfo(id: Long, username: String, phonenumber: String, address: String, email: String, city: String, district: String, ward: String) : Int = {
        db.withConnection { implicit connection =>
            SQL("UPDATE " + table_name + " SET name={name}, phone={phone}, address={address}, email={email}, city={city}, district={district}, ward={ward}  WHERE id={id}")
                .on("name" -> username).on("phone" -> phonenumber).on("address" -> address).on("email" -> email).on("id" -> id).on("city" -> city).on("district" -> district).on("ward" -> ward)
                .executeUpdate()
        }
    }

    def updateTransactionSuccess(id: Long): Int = {
        val now = System.currentTimeMillis()
        db.withConnection { implicit connection =>
            SQL("UPDATE " + table_name + " SET status=4, time_payment_success=" + now + " WHERE id={id}")
                .on("id" -> id)
                .executeUpdate()
        }
    }

    def resendEmail(id: Long, time_send_email: String): Int = {
        db.withConnection { implicit connection =>
            SQL("UPDATE " + table_name + " SET time_send_email={time_send_email}  WHERE id={id}")
              .on("time_send_email" -> time_send_email).on("id" -> id)
              .executeUpdate()
        }
    }
}
