package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import helpers.JsonExt
import models.{EventRequireInfo}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class EventRequireInfoDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val table_name = "event_require_info"
    val parsedEventRequireInfo = {
        get[Int]("id") ~
            get[Long]("event_id") ~
            get[Int]("index_sort") ~
            get[String]("question_vi") ~
            get[String]("question_en") ~
            get[String]("answer_type") ~
            get[Boolean]("required") ~
            get[Boolean]("enable") map {
            case id ~ event_id ~ index_sort ~ question_vi ~ question_en ~ answer_type ~ required ~ enable
            => EventRequireInfo(id, event_id, index_sort, question_vi, question_en, answer_type, required, enable)
        }
    }

//    def insert(eventRequireInfoAnswer: EventRequireInfoAnswer): Long = {
//        var result: Long = -1
//        db.withConnection { implicit connection =>
//            val id: Option[Long] = SQL("INSERT into " + table_name + "(id, event_id, order_id, event_require_info_id, answer) " +
//                "value({id}, {event_id}, {order_id}, {event_require_info_id}, {answer})")
//                .on("id" -> eventRequireInfoAnswer.id)
//                .on("event_id" -> eventRequireInfoAnswer.event_id)
//                .on("order_id" -> eventRequireInfoAnswer.order_id)
//                .on("event_require_info_id" -> eventRequireInfoAnswer.event_require_info_id)
//                .on("answer" -> eventRequireInfoAnswer.answer)
//                .executeInsert()
//
//            result = id match {
//                case Some(number) => number
//                case None => -1
//            }
//        }
//
//        result
//    }

    def getEventRequireInfoByEventId(event_id: Long) : List[EventRequireInfo] = {
        db.withConnection { implicit connection =>
            val parser: RowParser[EventRequireInfo] = Macro.namedParser[EventRequireInfo]
            val transaction = SQL("SELECT * FROM " + table_name + " WHERE enable = 1 AND event_id = {event_id}")
                .on("event_id" -> event_id)
                .as(parser.*)
            transaction
        }
    }
}
