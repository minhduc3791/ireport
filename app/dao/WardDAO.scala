package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.{District, Ward}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class WardDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parseWard = {
        get[String]("id") ~
            get[String]("name") ~
            get[String]("type_ward") ~
            get[String]("district_id") map {
            case id ~ name ~ type_ward ~ district_id
            => Ward(id, name, type_ward, district_id)
        }
    }

    def getDistrictByCityId(district_id: String): List[Ward] = {
        db.withConnection { implicit connection =>
            val wards = SQL("""SELECT * FROM ward WHERE district_id={district_id}""")
                .on("district_id" -> district_id)
                .as(parseWard.*)

            wards
        }
    }

    def getDistrictByCityId(district_id: List[String]): List[Ward] = {
        val district_id_string = "760,761"
        db.withConnection { implicit connection =>
            val wards = SQL("""SELECT * FROM ward WHERE district_id IN ({district_id_string})""")
                .on("district_id_string" -> district_id_string)
                .as(parseWard.*)

            var x = 1
            x = x + 3
            wards
        }
    }
}
