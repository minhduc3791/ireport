package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.{City, District, Ward}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class CityDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parseCity = {
        get[String]("id") ~
            get[String]("name") ~
            get[String]("type_city") map {
            case id ~ name ~ type_city
            => City(id, name, type_city)
        }
    }

    def getCities(): List[City] = {
        db.withConnection { implicit connection =>
            val cities = SQL("""SELECT * FROM city""")
                .as(parseCity.*)

            cities
        }
    }
}
