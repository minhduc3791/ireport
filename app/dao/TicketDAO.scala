package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.{EventTicketType, Ticket}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class TicketDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parsedTicket = {
        get[Long]("id") ~
            get[Long]("event_id") ~
            get[Long]("id_order") ~
            get[Long]("id_ticket_type") ~
            get[Long]("id_schedule") ~
            get[Int]("id_schedule_ticket_type") ~
            get[Int]("checkin_status") ~
            get[String]("note") ~
            get[String]("seat") map {
            case id ~ event_id ~ id_order ~ id_ticket_type ~ id_schedule ~ id_schedule_ticket_type ~ checkin_status ~ note ~ seat
            => Ticket(id, event_id, id_order, id_ticket_type, id_schedule, id_schedule_ticket_type, checkin_status, note, seat)
        }
    }

    def insert(ticket: Ticket): Long = {
        var result: Long = -1
        db.withConnection { implicit connection =>
            val id: Option[Long] = SQL("INSERT INTO ticket(event_id, id_order, id_ticket_type, id_schedule, id_schedule_ticket_type, checkin_status, note, seat) " +
                "value({event_id}, {id_order}, {id_ticket_type}, {id_schedule}, {id_schedule_ticket_type}, {checkin_status}, {note}, {seat})")
                .on("event_id" -> ticket.event_id).on("id_order" -> ticket.id_order).on("id_ticket_type" -> ticket.id_ticket_type).on("id_schedule" -> ticket.id_schedule)
                .on("id_schedule_ticket_type" -> ticket.id_schedule_ticket_type).on("checkin_status" -> ticket.checkin_status).on("note" -> ticket.note).on("seat" -> ticket.seat)
                .executeInsert()

            result = id match {
                case Some(number) => number
                case None => -1
            }
        }
        result

    }

    def getTicketByTransaction(id_order: List[Long]) = {
        val id_order_string = id_order.mkString(",")
        db.withConnection { implicit connection =>
            SQL("""SELECT * FROM ticket WHERE id_order IN ({id_order_string})""")
                .on("id_order_string" -> id_order_string)
                .as(parsedTicket.*)
        }
    }

    def getTicketByEvent(event_id: Long) = {
        db.withConnection { implicit connection =>
            SQL("""SELECT * FROM ticket WHERE event_id={event_id}""")
              .on("event_id" -> event_id)
              .as(parsedTicket.*)
        }
    }
}
