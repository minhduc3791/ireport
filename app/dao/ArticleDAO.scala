package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.Article
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class ArticleDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
  val parsedArticle = {
        get[Long]("article_id") ~
        get[Long]("article_bb_id") ~
        get[String]("title") ~
        get[String]("author") ~
        get[String]("link") ~
        get[String]("content") ~
        get[String]("publish_date") map {
      case article_id ~ article_bb_id ~ title ~ author ~ link ~ content ~ publish_date
      => Article(article_id, article_bb_id, title, author, link, content, publish_date)
    }
  }
}
