package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.{Voucher}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class VoucherDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parseVoucher = {
        get[Long]("voucher_id") ~
            get[Long]("event_id") ~
            get[Long]("voucher_group_id") ~
            get[String]("voucher_code") ~
            get[String]("email") ~
            get[String]("phonenumber") ~
            get[Int]("min_ticket") ~
            get[Int]("max_ticket") ~
            get[Int]("min_value") ~
            get[Int]("voucher_type") ~
            get[Int]("discount_value") ~
            get[Int]("max_discount") ~
            get[Long]("total_discount") ~
            get[Int]("voucher_count") ~
            get[Int]("voucher_used_count") ~
            get[Long]("start_time") ~
            get[Long]("end_time") ~
            get[Int]("enable") map {
            case voucher_id ~ event_id ~ voucher_group_id ~ voucher_code ~ email ~ phonenumber ~ min_ticket ~ max_ticket ~ min_value ~ voucher_type ~ discount_value ~ max_discount ~ total_discount ~ voucher_count
                ~ voucher_used_count ~ start_time ~ end_time ~ enable
            => Voucher(voucher_id, event_id, voucher_group_id, voucher_code, email, phonenumber, min_ticket, max_ticket, min_value, voucher_type, discount_value, max_discount, total_discount, voucher_count, voucher_used_count, start_time, end_time, enable)
        }
    }

    def verifyVoucher(voucher_code: String, event_id: Long): Voucher = {
        db.withConnection { implicit connection =>
            val vouchers = SQL("""SELECT * FROM event_voucher WHERE voucher_code={voucher_code} AND event_id={event_id} AND enable=1""")
                .on("voucher_code" -> voucher_code).on("event_id" -> event_id)
                .as(parseVoucher.*)

            if(vouchers.length > 0)
                vouchers(0)
            else
                null
        }
    }

    def getClaimedVoucher(event_id: Long, email: String, phonenumber: String): Voucher = {
        db.withConnection { implicit connection =>
            val vouchers = SQL("""SELECT * FROM event_voucher WHERE email={email} AND phonenumber={phonenumber} AND event_id={event_id} AND enable=0 LIMIT 1""")
                .on("email" -> email).on("phonenumber" -> phonenumber).on("event_id" -> event_id)
                .as(parseVoucher.*)

            if(vouchers.length > 0)
                vouchers(0)
            else
                null
        }
    }

    def getUnclaimVoucher(event_id: Long): Voucher = {
        db.withConnection { implicit connection =>
            val vouchers = SQL("""SELECT * FROM event_voucher WHERE event_id={event_id} AND enable=0 LIMIT 1""")
                .on("event_id" -> event_id)
                .as(parseVoucher.*)

            if(vouchers.length > 0)
                vouchers(0)
            else
                null
        }
    }

    def enableVoucher(voucher_code: String): Int = {
        db.withConnection { implicit connection =>
            SQL("""UPDATE event_voucher SET enable=1 WHERE voucher_code={voucher_code}""")
                .on("voucher_code" -> voucher_code)
                .executeUpdate()
        }
    }

    def getFreeVoucher(event_id: Long, email: String, phonenumber: String): Voucher = {
        db.withTransaction { implicit connection =>
            var voucher = getClaimedVoucher(event_id, email, phonenumber)
            if(voucher == null) {
                voucher = getUnclaimVoucher(event_id)

                if (voucher != null) {
                    enableVoucher(voucher.voucher_code)
                }
            }
            voucher
        }
    }

    def updateVoucher(voucher_used_count: Int, voucher_code: String): Int = {
        db.withConnection { implicit connection =>
            SQL("""UPDATE event_voucher SET voucher_used_count={voucher_used_count} WHERE voucher_code={voucher_code}""")
                .on("voucher_used_count" -> voucher_used_count).on("voucher_code" -> voucher_code)
                .executeUpdate()
        }
    }
}
