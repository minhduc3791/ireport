package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import helpers.JsonExt
import models.{EventRequireInfoAnswer}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class EventRequireInfoAnswerDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val table_name = "event_require_info_answer"
    val parsedEventRequireInfoAnswer = {
        get[Int]("id") ~
            get[Long]("event_id") ~
            get[Long]("order_id") ~
            get[Int]("event_require_info_id") ~
            get[String]("answer") map {
            case id ~ event_id ~ order_id ~ event_require_info_id ~ answer
            => EventRequireInfoAnswer(id, event_id, order_id, event_require_info_id, answer)
        }
    }

    def insert(eventRequireInfoAnswer: EventRequireInfoAnswer): Long = {
        var result: Long = -1
        db.withConnection { implicit connection =>
            val id: Option[Long] = SQL("INSERT into " + table_name + "(id, event_id, order_id, event_require_info_id, answer) " +
                "value({id}, {event_id}, {order_id}, {event_require_info_id}, {answer})")
                .on("id" -> eventRequireInfoAnswer.id)
                .on("event_id" -> eventRequireInfoAnswer.event_id)
                .on("order_id" -> eventRequireInfoAnswer.order_id)
                .on("event_require_info_id" -> eventRequireInfoAnswer.event_require_info_id)
                .on("answer" -> eventRequireInfoAnswer.answer)
                .executeInsert()

            result = id match {
                case Some(number) => number
                case None => -1
            }
        }

        result
    }

    def getEventRequireInfoAnswerByOrderId(order_id: Long) : List[EventRequireInfoAnswer] = {
        db.withConnection { implicit connection =>
            val parser: RowParser[EventRequireInfoAnswer] = Macro.namedParser[EventRequireInfoAnswer]
            val transaction = SQL("SELECT * FROM " + table_name + " where order_id = {order_id}")
                .on("order_id" -> order_id)
                .as(parser.*)
            transaction
        }
    }
}
