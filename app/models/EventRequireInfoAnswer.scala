package models

case class EventRequireInfoAnswer(
                       var id: Int,
                       var event_id: Long,
                       var order_id: Long,
                       var event_require_info_id: Int,
                       var answer: String
                     )
