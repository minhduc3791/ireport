package models

case class User(
                 var id: Long,
                 var phone: String,
                 var full_name: String,
                 var user_name: String,
                 var email: String,
                 var password: String,
                 var gender: String,
                 var dob: String,
                 var organization: String,
                 var time_create: Long,
                 var note: String,
                 var enable: Boolean,
                 var permission_group_id: Long
               )
