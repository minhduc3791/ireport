package models

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlElement, XmlRootElement}

@XmlRootElement(name = "shop")
@XmlAccessorType(XmlAccessType.FIELD)
case class OrderPayoo(
             var username: String,
             var shop_id: Long,
             var session: String,
             var shop_title: String,
             var shop_domain: String,
             var shop_back_url: String,
             var order_no: String,
             var order_cash_amount: String,
             var order_ship_date: String,
             var order_ship_days: Int,
             var order_description: String,
             var notify_url: String,
             var validity_time: String,
             var customer: CustomerPayoo,
            ){
    def this() = this("", 0, "", "", "", "", "", "", "", 0, "", "", "", null)
}
