package models

//result: 0: fail, 1: success, 2: redirect
case class Result(var code: Int, var data: String, var message: String)