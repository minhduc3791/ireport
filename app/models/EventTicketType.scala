package models

case class EventTicketType(
             var id: Long,
             var event_id: Long,
             var name_vi: String,
             var name_en: String,
             var total: Int,
             var price: Long,
             var min_buy: Int,
             var max_buy: Int,
             var start: Long,
             var end: Long,
             var start_date: String,
             var end_date: String,
             var start_time: String,
             var end_time: String,
             var type_ticket: String,
             var description_vi: String,
             var description_en: String,
             var note: String,
             var stop_ticketing: Int,
             var only_payment_office: Int,
             var schedule_text: String,
             var enable: Int,
             var total_ticket_sold: Int
            )/*{
  def this() = this(0, "", 0, "", "", "")
}*/
