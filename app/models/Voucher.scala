package models

case class Voucher(
                           var voucher_id: Long,
                           var event_id: Long,
                           var voucher_group_id: Long,
                           var voucher_code: String,
                           var email: String,
                           var phonenumber: String,
                           var min_ticket: Int,
                           var max_ticket: Int,
                           var min_value: Int,
                           var voucher_type: Int,
                           var discount_value: Int,
                           var max_discount: Int,
                           var total_discount: Long,
                           var voucher_count: Int,
                           var voucher_used_count: Int,
                           var start_time: Long,
                           var end_time: Long,
                           var enable: Int
            )
