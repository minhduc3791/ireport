package models

case class District(
             var id: String,
             var name: String,
             var type_district: String,
             var city_id: String,
             var shipment_price: Int
            )/*{
  def this() = this(0, "", 0, "", "", "")
}*/
