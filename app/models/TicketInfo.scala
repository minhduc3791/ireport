package models

case class TicketInfo(
             var id_ticket_type: Long,
             var name_ticket_type: String,
             var name_vi_ticket_type: String,
             var name_en_ticket_type: String,
             var id_schedule: Long,
             var id_schedule_ticket_type: Long,
             var number: Int,
             var revenue_before: Int,
             var revenue_discount: Int,
             var revenue_after: Int,
             var seats_info: String
            )/*{
  def this() = this(0, "", 0, "", "", "")
}*/
