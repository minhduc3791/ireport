package models

case class VoucherGroup(
                           var voucher_group_id: Long,
                           var event_id: Long,
                           var voucher_group_type: Int,
                           var voucher_group_name: String,
                           var voucher_group_name_en: String,
                           var voucher_group_note: String,
                           var voucher_group_note_en: String,
                           var n_ticket_tobuy: Int,
                           var ticket_id_tobuy: Long,
                           var n_percent_discount: Double,
                           var n_ticket_toget: Int,
                           var ticket_id_toget: Long,
                           var start_time: Long,
                           var end_time: Long,
                           var use_code: Int,
                           var use_condition: Int
            )
