package models
import org.joda.time.{DateTime, DateTimeZone}

case class SendgridEventNoti(
             var id: Long,
             var order_id: Long,
             var event_id: Long,
             var order_status: Int,
             var email_status: Int,
             var email_time: DateTime,
            )