package models

case class Ward(
             var id: String,
             var name: String,
             var type_ward: String,
             var district_id: String
            )/*{
  def this() = this(0, "", 0, "", "", "")
}*/
