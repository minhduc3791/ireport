package models

case class TransactionSuccess (
                             var banner: String,
                             var email: String,
                             var name_vi: String,
                             var name_en: String,
                             var venue: String,
                             var address: String,
                             var order_id: Long,
                             var time_create: Long,
                             var ticket_info: String,
                             var revenue_after: Int,
                             var revenue_before: Int,
                             var revenue_discount: Int,
                             var pay_type: Int,
                   )