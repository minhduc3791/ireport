package models

case class Seat(
             var seat_id: Long,
             var event_id: Long,
             var seat_map_id: Long,
             var seat_name_en: String,
             var seat_name_vi: String,
             var ticket_type_id: Long,
             var is_enable: Int,
             var is_locked: Int,
             var is_couple: Int,  //(-1, 0 , 1)
             var seat_note_en: String,
             var seat_note_vi: String,
             var is_sold: Int,
             var locked_time: Long,
             var related_seats: String
            )