package models

case class City(
             var id: String,
             var name: String,
             var type_city: String
            )