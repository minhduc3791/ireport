package models

case class Ticket(
             var id: Long,
             var event_id: Long,
             var id_order: Long,
             var id_ticket_type: Long,
             var id_schedule: Long,
             var id_schedule_ticket_type: Long,
             var checkin_status: Int,
             var note: String,
             var seat: String,
            )/*{
  def this() = this(0, "", 0, "", "", "")
}*/
