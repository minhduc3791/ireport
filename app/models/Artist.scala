package models

case class Artist (
                  var rank: Long,
                  var artist_name: String,
                  var h_peak_rank: Long,
                  var h_last_week: Long,
                  var h_weeks_on_chart: Long,
                  var image: String
                 )/*{
  def this() = this(0, "", "", "")
}*/
