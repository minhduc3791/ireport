package models

case class EventRequireInfo(
                       var id: Int,
                       var event_id: Long,
                       var index_sort: Int,
                       var question_vi: String,
                       var question_en: String,
                       var answer_type: String,
                       var required: Boolean,
                       var enable: Boolean
                     )
