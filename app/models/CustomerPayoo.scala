package models

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType}

@XmlAccessorType(XmlAccessType.FIELD)
case class CustomerPayoo (
                  var name: String,
                  var phone: String,
                  var address: String,
                  var email: String
                 ){
    def this() = this("", "", "", "")
}
