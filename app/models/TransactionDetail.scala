package models

case class TransactionDetail (
                             var id: Long,
                             var pay_type: Int, //1: online - 2: cod - 3: offline
                             var time_create: Long,
                             var time_payment_success: Long,
                             var status: Int, // 1: created - 2: pending(update after call to online payment)
                             // 3: succeed - 4: failed
                             var name: String,
                             var email: String,
                             var phone: String,
                             var city_name: Option[String],
                             var district: Option[String],
                             var ward: Option[String],
                             var ticket_info: String,
                             var revenue_before: Long,
                             var revenue_discount: Long,
                             var revenue_after: Long,
                             var apply_voucher: Option[String],
                             var event_id: Long,
                             var event_name_vi: String,
                             var event_name_en: String,
                             var note: String,
                             var transaction_address: String,
                             var event_address: String

                   )/*{
  def this() = this(0, "", "")
}*/
