package models

case class Event(
                   var id: Long,
                   var link: String,
                   var name_vi: String,
                   var name_en: String,
                   var event_type: String,
                   var position: String,
                   var address: String,
                   var location: String,
                   var venue: String,
                   var district: String,
                   var city: String,
                   var banner: String,
                   var poster: String,
                   var sit_map: String,
                   var enable: Boolean,
                   var date_show: Long,
                   var description_vi: String,
                   var description_en: String,
                   var email_contact: String,
                   var phone_contact: String,
                   var list_organizer: String,
                   var list_condition: String,
                   var time_remind_payment: Int,
                   var time_cancel_payment: Int
                )


case class ListEvent(var id: Long, var name_vi: String, var name_en: String)

