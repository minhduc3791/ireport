package models

case class SeatMap(
             var seat_map_id: String,
             var seat_map_name_en: String,
             var seat_map_name_vi: String,
             var seat_map_note_en: String,
             var seat_map_note_vi: String,
             var is_enable: String
            )
