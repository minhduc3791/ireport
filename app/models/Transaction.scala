package models

case class Transaction (
                   var id: Long,
                   var event_id: Long,
                   var gender: Int,
                   var name: String,
                   var phone: String,
                   var email: String,
                   var company_name: String,
                   var company_address: String,
                   var company_tax_code: String,
                   var city: String,
                   var district: String,
                   var ward: String,
                   var address: String,
                   var user: Long,
                   var note: String,
                   var pay_type: Int,
                   var apply_voucher: String, // {"voucher_group_id": 1, "voucher_code": "xyz"}
                   var revenue_before: Int,
                   var revenue_discount: Int,
                   var revenue_after: Int,
                   var time_create: Long,
                   var invoice_vat: Int,
                   var status: Int,
                   var ticket_info: String,
                   var time_payment_success: Long,
                   var time_send_email: String,
                   var language_code: Int
                   )/*{
  def this() = this(0, "", "")
}*/
