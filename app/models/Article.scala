package models

case class Article (
                  var article_id: Long,
                  var article_bb_id: Long,
                  var title: String,
                  var author: String,
                  var link: String,
                  var content: String,
                  var publish_date: String
                 )/*{
  def this() = this(0, "", "", "")
}*/
