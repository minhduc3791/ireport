package helpers

import play.api.data.Form
import play.api.mvc.{Action, Controller}

object Auth extends Controller {
    def doLogin() = {}

    def check(username: String, password: String) = {
        (username == "admin" && password == "1234")
    }

    def login = Action { implicit request =>
        Ok(views.html.login(""))
    }

    /*def authenticate = Action { implicit request =>
        loginForm.bindFromRequest.fold(
            formWithErrors => BadRequest(html.login(formWithErrors)),
            user => Redirect(routes.Application.index).withSession(Security.username -> user._1)
        )
    }*/

    def logout = Action {
        Redirect("").withNewSession.flashing(
            "success" -> "You are now logged out."
        )
    }
}


