package helpers

import java.security.MessageDigest
import java.time._

import models.TicketInfo
import net.glxn.qrgen.QRCode
import net.glxn.qrgen.image.ImageType
import org.apache.commons.codec.binary.Base64
import play.api.libs.json.{JsPath, Json, Reads, Writes}
import play.api.libs.functional.syntax._
import org.joda.time.{DateTime, DateTimeZone}
import org.joda.time.format.DateTimeFormat

import scala.collection.mutable.ListBuffer

object Common {
  val formatter = java.text.NumberFormat.getIntegerInstance
  var lastTimeMillis: Long = -1L

  def convertTicketInfo(jsonString: String): List[TicketInfo] = {
    val _ticket_info = new ListBuffer[TicketInfo]
    val ticketInfos = JsonExt.gson.fromJson(jsonString, classOf[TicketInfo])
    /*for(ticketInfo <- ticketInfos){
        _ticket_info += ticketInfo
    }*/

    _ticket_info.toList
  }

  def generate(data: String): String = {
    val prefix = "data:image/gif;base64,"
    val encodedByte = Base64.encodeBase64(QRCode.from(data)
      .to(ImageType.GIF)
      .withSize(200, 200)
      .withCharset("UTF-8")
      .stream().toByteArray())

    prefix + new String(encodedByte)
  }

  def convertTimestamp(timestap: Long): String = {
    if (timestap > 0)
      new DateTime(timestap).toDateTime.toString("yyyyMMddHHmmss")
    else
      ""
  }

  def convertTimestampVN(timestap: Long): String = {
    if (timestap > 0)
      new DateTime(timestap).toDateTime.toString("dd/MM/yyyy HH:mm:ss")
    else
      ""
  }

  def getXMLValue(node: scala.xml.NodeSeq, att: String): String = {
    (node \\ att headOption).map(_.text).map { res =>
      res
    }.getOrElse {
      ""
    }
  }

  def convertTimestamp(timestap: Long, format: String): String = {
    if (timestap > 0)
      new DateTime(timestap).toDateTime.toString(format)
    else
      ""
  }

  def formatCurrency(value: Long): String = {
    formatter.format(value)
  }

  def generate(t: String, text: String): String = {
    val checksum = MessageDigest.getInstance(t).digest(text.getBytes("UTF-8"))
    checksum.map("%02x" format _).mkString
  }

  def generateUniqueId: Long = {
    var time = System.currentTimeMillis
    if (time <= lastTimeMillis) time = lastTimeMillis + 1
    lastTimeMillis = time
    time
  }

  def mapPaytype(pay_type: Int): String = {
    pay_type match {
      case 2 => "Chuyển khoản"
      case 3 => "Visa/Master"
      case 4 => "COD"
      case 5 => "Thanh toán tại quầy"
      case 6 => "Mua vé trực tiếp"
      case 7 => "ATM"
      case 8 => "Thanh toán bằng voucher"
      case _ => "Thanh toán tại quầy"
    }
  }

  def mapTransactionStatus(status: Int): String = {
    status match {
      case 0 => "Đã đặt chỗ"
      case 1 => "Đã xác nhận"
      case 2 => "Đã đóng gói"
      case 3 => "Đang vận chuyển"
      case 4 => "Thành công"
      case 5 => "Thanh toán thất bại"
      case 6 => "Hủy đơn hàng"
      case 7 => "Đã hoàn tiền"
      case _ => "Đang xử lý"
    }
  }

  def _mapTransactionStatus(status: Int): String = {
    status match {
      case 0 => "Đang xử lý"
      case 1 => "Đang xử lý"
      case 2 => "Đang xử lý"
      case 3 => "Đang xử lý"
      case 4 => "Thành công"
      case 5 => "Thất bại"
      case 6 => "Thất bại"
      case 7 => "Thất bại"
      case _ => "Đang xử lý"
    }
  }

  def mapCheckinStatus(status: Int): String = {
    status match {
      case 0 => "Chưa checkin"
      case 1 => "Đã checkin"
      case _ => "Chưa checkin"
    }
  }

  def currencyFormat(value: Long): String = {
    import java.text.DecimalFormat
    import java.text.NumberFormat
    import java.util.Locale
    val formatter = NumberFormat.getInstance(Locale.US).asInstanceOf[DecimalFormat]
    val symbols = formatter.getDecimalFormatSymbols

    symbols.setGroupingSeparator(',')
    formatter.setDecimalFormatSymbols(symbols)
    formatter.format(value)
  }

  def mapTransactionStatusStyle(status: Int): String = {
    status match {
      case 0 => "green_report"
      case 1 => "green_report"
      case 2 => "green_report"
      case 3 => "green_report"
      case 4 => ""
      case 5 => "red_report"
      case 6 => "red_report"
      case 7 => "red_report"
      case _ => "green_report"
    }
  }

  def CheckClientIp(ip: String): Boolean = ip match {
    case "203.205.26.14555" => false
    case "58.187.170.11" => false
    case _ => true
  }

  def getStartOfDay(timestamp: Long): Long = {
    val dateFromEpoch = LocalDateTime
      .ofInstant(Instant.ofEpochSecond(timestamp / 1000), ZoneId.of("UTC"))
      .`with`(LocalTime.MIN)

    dateFromEpoch.toEpochSecond(ZoneOffset.UTC)
  }

  def buildInsertQuery(className: Class[_]): String = {
    var queryString = "INSERT INTO " + className.getSimpleName + "("
    var first = 0
    className.getDeclaredFields foreach { s =>
      s.setAccessible(true)
      if (first == 0)
        first = 1
      else
        queryString += ","
      queryString += s.getName
    }

    queryString += ") value("
    first = 0
    className.getDeclaredFields foreach { s =>
      s.setAccessible(true)
      if (first == 0)
        first = 1
      else
        queryString += ","
      queryString += "{" + s.getName + "}"
    }

    queryString += ")"
    queryString
  }

  def emailStatus(status: Int): String = {
    status match {
      case 0 => "Pending"
      case 1 => "Delivered"
      case 2 => "Opened"
      case 3 => "Dropped"
      case 4 => "Deferred"
      case 5 => "Bounce"
      case 6 => "Spam report"
    }
  }


}
