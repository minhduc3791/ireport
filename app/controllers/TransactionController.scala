package controllers

import java.awt.Color
import java.io.{File, FileOutputStream}

import javax.inject.Inject
import dao._
import helpers.{Common, JsonExt}
import models._
import org.apache.poi.hssf.usermodel.HSSFCellStyle
import org.apache.poi.hssf.usermodel.HSSFFont
import org.apache.poi.hssf.util.HSSFColor
import org.apache.poi.xssf.usermodel.XSSFColor
import org.apache.poi.ss.usermodel.{BorderStyle, FillPatternType, HorizontalAlignment}
import org.apache.poi.xslf.usermodel.VerticalAlignment
import org.apache.poi.xssf.usermodel._
import play.api.db.Database
import play.api.libs.ws.WSClient
import play.api.mvc.{BaseController, ControllerComponents}
import security.Secured
import services.PayooService

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.language.postfixOps

case class UpdateTransactionNote(var id: Long, var note: String)

case class UpdateTransactionStatus(var id: Long, var status: Int)

case class UpdateTransactionUserInfo(var id: Long, var name: String, var phone: String, var address: String, var email: String, var city: String, var district: String, var ward: String)

case class VerifyVoucher(var voucher_code: String, var event_id: Long)

case class AppliedVoucher(var voucher_code: String, var voucher_group_id: Long)

case class ResendEmail(var id: Long, email_phase: Long)

class TransactionController @Inject()(ws: WSClient, db: Database, val controllerComponents: ControllerComponents) extends BaseController with Secured {
  val transactionDAO: TransactionDAO = new TransactionDAO(db, controllerComponents)
  val eventRequireInfoAnswerDAO: EventRequireInfoAnswerDAO = new EventRequireInfoAnswerDAO(db, controllerComponents)
  val eventRequireInfoDAO: EventRequireInfoDAO = new EventRequireInfoDAO(db, controllerComponents)
  val transactionDetailDAO: TransactionDetailDAO = new TransactionDetailDAO(db, controllerComponents)
  val evenTicketTypeDAO: EventTicketTypeDAO = new EventTicketTypeDAO(db, controllerComponents)
  val ticketDAO: TicketDAO = new TicketDAO(db, controllerComponents)
  val eventDAO: EventDAO = new EventDAO(db, controllerComponents)
  val distDAO: DistrictDAO = new DistrictDAO(db, controllerComponents)
  val wardDAO: WardDAO = new WardDAO(db, controllerComponents)
  val voucherDAO: VoucherDAO = new VoucherDAO(db, controllerComponents)
  val seatDAO: SeatDAO = new SeatDAO(db, controllerComponents)
  val sendgridEventNotiDAO: SendgridEventNotiDAO = new SendgridEventNotiDAO(db, controllerComponents)
  val payooService: PayooService = new PayooService(ws, db, controllerComponents)

//  val admins = Array[String]("khoatang", "du.duong", "hanh.do")

  def resend = Action { request =>
    val t = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[ResendEmail])
    val transaction = transactionDAO.getTransactionsById(t.id)
    var permission_group_id = 0.toInt
    request.session.get("permission_group_id").map { id => permission_group_id = id.toInt; }
    var admin = -1
    if(permission_group_id == 3) {
      admin = 1
    }
    request.session.get("username").map { username =>
      if (admin >= 0) {
        val emails = transaction.time_send_email.split(" ").filter(data => data.indexOf("." + t.email_phase + ".") < 0)
        Ok(transactionDAO.resendEmail(t.id, emails.mkString(" ")).toString)
      } else {
        Forbidden("You're not allowed to access this resource.")
      }
    }.getOrElse {
      Ok(views.html.login("/" + transaction.event_id))
    }
  }

  def reportTicket(event_id: Long) = Action { request =>
    request.session.get("username").map { username =>
      val event = eventDAO.getEventByEventId(event_id)
      val list_event = eventDAO.getListEvent()
      val transactions = transactionDAO.getTransactionsByEvent(event_id)
      val ticket_types = evenTicketTypeDAO.getTicketByEvent(event_id)

      Ok(views.html.report_index()(views.html.report_ticket(transactions, ticket_types, event, list_event)))
    }.getOrElse {
      Ok(views.html.login("/" + event_id))
    }
  }

  def verifyVoucher = Action { request =>
    val data = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[VerifyVoucher])
    val voucher = voucherDAO.verifyVoucher(data.voucher_code, data.event_id)
    val result = new models.Result(-1, "", "")
    if (voucher != null && (voucher.voucher_count == 0 || voucher.voucher_used_count + 1 <= voucher.voucher_count)) {
      //voucherDAO.updateVoucher(voucher.voucher_used_count + 1, voucher.voucher_code)
      result.code = 1
      result.data = JsonExt.toRawJson(voucher)
    }

    Ok(JsonExt.toRawJson(result))
  }

  def createTransaction = Action { request =>
    val remoteAddress: String = request.remoteAddress
    val result = new models.Result(0, "", "")
    val transaction_id = Common.generateUniqueId
    val transaction = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[Transaction])
    transaction.id = transaction_id

    println("-----------------create order-----------------")
    println(transaction.id + ", event_id:: " + transaction.event_id + ", " + transaction.name)
    println(remoteAddress)
    if (!Common.CheckClientIp(remoteAddress.toString)) {
      println("Failed")
      result.code = -1
      result.data = "Your connection cannot execute this transaction. Please try again or contact phone number 1900 6363 02 press 2 or via email support@dreamspass.vn"
      result.message = "Failed"
    } else {
      val ticket_infos = JsonExt.gson.fromJson(transaction.ticket_info, classOf[Array[TicketInfo]])
      var exceed_ticket: Long = -1L
      var sold_seat: Long = -1L
      for (ticket_info <- ticket_infos) {
        val verify = evenTicketTypeDAO.verify(ticket_info.id_ticket_type)
        if (verify.total_ticket_sold + ticket_info.number > verify.total) {
          exceed_ticket = ticket_info.id_ticket_type
        }

        val seats_info = JsonExt.gson.fromJson(ticket_info.seats_info, classOf[Array[Seat]])
        for (i <- 0 to ticket_info.number - 1) {
          var seat_info = ""
          if (seats_info.length > 0) {
            seat_info = JsonExt.toRawJson(seats_info(i))
            val seat = seatDAO.getSeat(seats_info(i).seat_id, seats_info(i).seat_map_id, seats_info(i).event_id)
            if (seat.is_sold == 1) {
              sold_seat = seat.seat_id
            }
          }
        }
      }

      if (exceed_ticket >= 0) {
        result.code = -1
        result.data = exceed_ticket.toString
        result.message = "Ticket exceed"
      } else {
        if (sold_seat >= 0) {
          result.code = -1
          result.data = sold_seat.toString
          result.message = "Seat sold"
        } else {
          try {
            var new_tickets_info = new ArrayBuffer[TicketInfo]

            for (ticket_info <- ticket_infos) {
              evenTicketTypeDAO.buyTicket(ticket_info.id_ticket_type, ticket_info.number)
              val seats_info = JsonExt.gson.fromJson(ticket_info.seats_info, classOf[Array[Seat]])
              var count = 0
              for (i <- 0 to ticket_info.number - 1) {
                var seat_info = ""
                if (seats_info.length > 0) {
                  seat_info = JsonExt.toRawJson(seats_info(count))
                  seatDAO.lockSeat(seats_info(count).seat_id, System.currentTimeMillis())
                  if (transaction.pay_type == 2 || transaction.status == 4)
                    seatDAO.buySeat(seats_info(count).seat_id)
                }
                val ticket = new Ticket(-1, transaction.event_id, transaction_id, ticket_info.id_ticket_type, ticket_info.id_schedule, ticket_info.id_schedule_ticket_type, 0, "", seat_info)

                ticketDAO.insert(ticket)
                count += 1
              }
            }

            val appliedVouchers = JsonExt.gson.fromJson(transaction.apply_voucher, classOf[Array[AppliedVoucher]])
            for (appliedVoucher <- appliedVouchers) {
              if (appliedVoucher.voucher_code.length > 0) {
                val voucher = voucherDAO.verifyVoucher(appliedVoucher.voucher_code, transaction.event_id)
                if (voucher != null && (voucher.voucher_count == 0 || voucher.voucher_used_count + 1 <= voucher.voucher_count)) {
                  voucherDAO.updateVoucher(voucher.voucher_used_count + 1, voucher.voucher_code)
                } else {
                  exceed_ticket = -2
                }
              }
            }
            //todo: recalculate transaction value
            //todo: roll back
            if (exceed_ticket == -2) {
              result.code = -1
              result.data = "The data you submit does not match our data, please try again"
            } else {
              transactionDAO.insert(transaction)

              //insert require info
              try {
                val require_info_answer_list = JsonExt.gson.fromJson(request.body.asJson.get("require_info_answer_list").toString, classOf[Array[EventRequireInfoAnswer]])

                for (requireInfo <- require_info_answer_list) {
                  requireInfo.order_id = transaction_id;
                  eventRequireInfoAnswerDAO.insert(requireInfo);
                }
              } catch {
                case e: Exception => println("dont have require_info_answer_list")
              }

              if (transaction.pay_type == 3) {
                result.code = 2
                result.data = payooService.createOrder("cc-payment", transaction_id.toString, transaction.revenue_after.toString, transaction.name, transaction.phone, transaction.address, transaction.email)
                result.message = "Success"
              }
              else if (transaction.pay_type == 7) {
                result.code = 2
                result.data = payooService.createOrder("bank-payment", transaction_id.toString, transaction.revenue_after.toString, transaction.name, transaction.phone, transaction.address, transaction.email)
                result.message = "Success"
              }
              else if (transaction.pay_type == 9) {
                result.code = 3
                result.data = transaction_id.toString
                result.message = "Success"
              }
              else {
                result.code = 1
                result.data = transaction_id.toString
                result.message = "Success"
              }
            }
          } catch {
            case e: Exception =>
              result.code = -1
              result.data = e.getMessage
              result.message = "Failed"
            case _: Throwable =>
              result.code = -1
              result.data = "Unknown error, please try again"
              result.message = "Failed"
          }
        }
      }
    }
    println("-----------------end create order-----------------")

    Ok(JsonExt.toRawJson(result))
  }

  def detail(id: Long) = Action { request =>
    request.session.get("username").map { username =>

      var permission_group_id = 0.toInt
      request.session.get("permission_group_id").map { id => permission_group_id = id.toInt; }
      var isAdmin = 1
      //permission_group_id business development
      if(permission_group_id != 3){
        isAdmin = -1
      }

      val transaction = transactionDetailDAO.getTransactionDetailById(id)
      val tickets = ticketDAO.getTicketByTransaction(List(id))
      val ticket_types = evenTicketTypeDAO.getTicketByEvent(transaction.event_id)
      val sendgridEventNotis = sendgridEventNotiDAO.getSendgridEventNoti(id)
      val districts = distDAO.getDistrictByCityId("79")

      val eventRequireInfos = eventRequireInfoDAO.getEventRequireInfoByEventId(transaction.event_id);
      val eventRequireInfoAnswers = eventRequireInfoAnswerDAO.getEventRequireInfoAnswerByOrderId(id);

      var wards = new ArrayBuffer[Ward]

      districts.foreach(district => wardDAO.getDistrictByCityId(district.id).foreach(w => wards = wards :+ w))

      if(permission_group_id == 4){
        Ok(views.html.login("/" + id))
      }

      Ok(views.html.report_index()(views.html.report_transaction_detail(username, transaction, tickets, ticket_types, districts, wards, isAdmin, sendgridEventNotis, eventRequireInfos, eventRequireInfoAnswers)))
    }.getOrElse {
      Ok(views.html.login("/" + id))
    }
  }

  def getTransactions(event_id: Long) = Action { request =>
    val trans = transactionDAO.getTransactionsByEvent(event_id)
    val trans_id = trans.map(t => t.id)
    val ticket_types = evenTicketTypeDAO.getTicketByEvent(event_id)
    val tickets = ticketDAO.getTicketByTransaction(trans_id)
    val res = """{"data": {"ticket_types":""" + JsonExt.toRawJson(ticket_types) + """, "transactions":""" + JsonExt.toRawJson(trans) + """, "tickets":""" + JsonExt.toRawJson(tickets) + """}}"""
    Ok(res)
  }

  def report(event_id: Long) = Action { request =>
    request.session.get("username").map { username =>
      var user_id = 0.toLong
      var permission_group_id = 0.toInt
      request.session.get("user_id").map { id => user_id = id.toLong; }
      request.session.get("permission_group_id").map { id => permission_group_id = id.toInt; }
      println("---272---" + user_id)
      var isAdmin = 1
      val trans = transactionDAO.getTransactionsByEvent(event_id)
      var event = eventDAO.getEventByEventId(event_id)
      val ticket_types = evenTicketTypeDAO.getTicketByEvent(event_id)
      var list_event = eventDAO.getListEvent();
      //permission_group_id business development
      if(permission_group_id != 3){
        list_event = eventDAO.getListEventByUser(user_id)
        isAdmin = -1
      }
      val districts = distDAO.getDistrictByCityId("79")

      var checkEvent = false
      for (ievent <- list_event) {
        if (event.id == ievent.id && !checkEvent) {
          checkEvent = true
        }
        ievent.name_vi = ievent.name_vi.replaceAll("<br>", " ");
      }

      if (!checkEvent) {
        event = eventDAO.getEventByEventId(list_event.head.id);
      }

      Ok(views.html.report_index()(views.html.report_transaction(username, trans, ticket_types, event, list_event, districts, isAdmin, permission_group_id)))
    }.getOrElse {
      Ok(views.html.login("/" + event_id))
    }
  }

  def updateTransactionStatus = Action { request =>
    val t = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[UpdateTransactionStatus])
    val transaction = transactionDAO.getTransactionsById(t.id)
    var permission_group_id = 0.toInt
    request.session.get("permission_group_id").map { id => permission_group_id = id.toInt; }
    var admin = -1
    if(permission_group_id == 3) {
      admin = 1
    }
    request.session.get("username").map { username =>
      if (admin >= 0) {
        if (t.status > 4) {
          //rollback
          val tickets_info = JsonExt.gson.fromJson(transaction.ticket_info, classOf[Array[TicketInfo]])
          for (ticket_info <- tickets_info) {
            //rollback ticket count
            evenTicketTypeDAO.buyTicket(ticket_info.id_ticket_type, -ticket_info.number)
            val seats_info = JsonExt.gson.fromJson(ticket_info.seats_info, classOf[Array[Seat]])
            if (seats_info.length > 0) {
              for (seat_info <- seats_info) {
                //roleback seat sold
                seatDAO.unbuySeat(seat_info.seat_id)
              }
            }
          }
        }
        Ok(transactionDAO.updateTransactionStatus(t.id, t.status).toString)
      } else {
        Forbidden("You're not allowed to access this resource.")
      }
    }.getOrElse {
      Ok(views.html.login("/" + transaction.event_id))
    }
  }

  def _updateTransactionStatus = Action { request =>
    val t = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[UpdateTransactionStatus])
    val transaction = transactionDAO.getTransactionsById(t.id)
    if (t.status > 4) {
      //rollback
      val tickets_info = JsonExt.gson.fromJson(transaction.ticket_info, classOf[Array[TicketInfo]])
      for (ticket_info <- tickets_info) {
        //rollback ticket count
        evenTicketTypeDAO.buyTicket(ticket_info.id_ticket_type, -ticket_info.number)
        val seats_info = JsonExt.gson.fromJson(ticket_info.seats_info, classOf[Array[Seat]])
        if (seats_info.length > 0) {
          for (seat_info <- seats_info) {
            //roleback seat sold
            seatDAO.unbuySeat(seat_info.seat_id)
          }
        }
      }
    }
    Ok(transactionDAO.updateTransactionStatus(t.id, t.status).toString)
  }

  def updateTransactionNote = Action { request =>
    val t = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[UpdateTransactionNote])
    val transaction = transactionDAO.getTransactionsById(t.id)
    var permission_group_id = 0.toInt
    request.session.get("permission_group_id").map { id => permission_group_id = id.toInt; }
    var admin = -1
    if(permission_group_id == 3) {
      admin = 1
    }
    request.session.get("username").map { username =>
      if (admin >= 0) {
        Ok(transactionDAO.updateTransactionNote(t.id, t.note).toString)
      } else {
        Forbidden("You're not allowed to access this resource.")
      }
    }.getOrElse {
      Ok(views.html.login("/" + transaction.event_id))
    }
  }

  def updateTransactionUserInfo = Action { request =>
    val u = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[UpdateTransactionUserInfo])
    val transaction = transactionDAO.getTransactionsById(u.id)
    var permission_group_id = 0.toInt
    request.session.get("permission_group_id").map { id => permission_group_id = id.toInt; }
    var admin = -1
    if(permission_group_id == 3) {
      admin = 1
    }
    request.session.get("username").map { username =>
      if (admin >= 0) {
        Ok(transactionDAO.updateTransactionUserInfo(u.id, u.name, u.phone, u.address, u.email, u.city, u.district, u.ward).toString)
      } else {
        Forbidden("You're not allowed to access this resource.")
      }
    }.getOrElse {
      Ok(views.html.login("/" + transaction.event_id))
    }
  }

  def exportReport = Action { request =>
    val is_admin = request.queryString.get("is_admin").get(0).toInt
    val event_id = request.queryString.get("event_id").get(0).toLong
    val start_date = request.queryString.get("start_date").get(0).toLong
    val end_date = request.queryString.get("end_date").get(0).toLong + 24 * 60 * 60 * 1000
    val ticket_type = request.queryString.get("ticket_type").get(0).toLong
    val pay_type = request.queryString.get("pay_type").get(0).toString
    val status = request.queryString.get("status").get(0).toInt

    val trans = transactionDAO.getTransactionsWithFilters(is_admin, event_id, start_date, end_date, pay_type, status, ticket_type)
    val ticket_types = evenTicketTypeDAO.getTicketByEvent(event_id)
    val tickets = ticketDAO.getTicketByEvent(event_id)

    val eventRequireInfos = eventRequireInfoDAO.getEventRequireInfoByEventId(event_id);

    var data_ticket_type = Array.fill(ticket_types.length) {
      0
    }

    var header_name_vi = Array[String]("Trạng thái", "Đơn hàng", "Giờ đặt", "Tên", "Email", "Số điện thoại", "Địa chỉ")
    ticket_types.foreach { ticket_type =>
      header_name_vi = header_name_vi ++ Array[String](ticket_type.name_vi)
    }
    header_name_vi = header_name_vi ++ Array[String]("Thông tin ghế", "Voucher Code", "Giảm giá", "Thành tiền", "Phương thức", "Ghi chú", "Trạng Thái Checkin")
    eventRequireInfos.foreach { require_info =>
      header_name_vi = header_name_vi ++ Array[String](require_info.question_vi)
    }

    var header_name_en = Array[String]("Status", "Order ID", "Purchasing Date and Time", "Payment Date", "Name", "Email", "Phone number", "Ticket Quantity")
    ticket_types.foreach { ticket_type =>
      header_name_en = header_name_en ++ Array[String](ticket_type.name_en)
    }
    header_name_en = header_name_en ++ Array[String]("Subtotal", "Discount Value", "Seat Info", "Voucher Code / Promotion", "Grand Total", "Payment Method", "Note", "Check-in Status")
    eventRequireInfos.foreach { require_info =>
      header_name_en = header_name_en ++ Array[String](require_info.question_vi)
    }

    var subheader_name_vi = Array[String]("")
    var subheader_name_en = Array[String]("TOTAL", "", "", "", "", "", "")

    val file_name = "report" + Common.convertTimestamp(System.currentTimeMillis(), "ddMMyyyy") + ".xlsx"
    val file = new File(new File(".").getAbsolutePath() + "/" + file_name)
    val fileOut = new FileOutputStream(file)
    val wb = new XSSFWorkbook
    val sheet = wb.createSheet("Sheet1")
    var rNum = 0
    var row = sheet.createRow(rNum)
    var cNum = 0
    val cell = row.createCell(cNum)
    var headerStyle = wb.createCellStyle
    var subHeaderStyle = wb.createCellStyle
    var dataCellStyle = wb.createCellStyle
    var numberStyle = wb.createCellStyle
    val headerColor = new XSSFColor(new Color(234, 209, 220))
    val subHeaderColor = new XSSFColor(new Color(255, 242, 204))
    val format = wb.createDataFormat

    headerStyle.setFillForegroundColor(headerColor)
    headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND)
    headerStyle.setBorderBottom(BorderStyle.THIN)
    headerStyle.setBorderTop(BorderStyle.THIN)
    headerStyle.setBorderLeft(BorderStyle.THIN)
    headerStyle.setBorderRight(BorderStyle.THIN)

    subHeaderStyle.setFillForegroundColor(subHeaderColor)
    subHeaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND)
    subHeaderStyle.setBorderBottom(BorderStyle.THIN)
    subHeaderStyle.setBorderTop(BorderStyle.THIN)
    subHeaderStyle.setBorderLeft(BorderStyle.THIN)
    subHeaderStyle.setBorderRight(BorderStyle.THIN)

    numberStyle.setAlignment(HorizontalAlignment.RIGHT)
    numberStyle.setDataFormat(format.getFormat("#,##0"))

    val font = wb.createFont
    font.setBold(true)
    headerStyle.setFont(font)
    subHeaderStyle.setFont(font)
    dataCellStyle.setFont(font)

    header_name_en.foreach { name =>
      val c = row.createCell(cNum)
      c.setCellValue(name)
      c.setCellStyle(headerStyle)

      cNum += 1
    }

    /*start subheader*/
    rNum += 1
    cNum = 0
    row = sheet.createRow(rNum)

    subheader_name_en.foreach { name =>
      val c = row.createCell(cNum)
      c.setCellValue(name)
      c.setCellStyle(subHeaderStyle)

      cNum += 1
    }

    var total_ticket = 0
    var total_revenue_before = 0
    var total_revenue_discount = 0
    var total_revenue_after = 0
    trans.foreach { tran =>
      total_revenue_before += tran.revenue_before
      total_revenue_discount += tran.revenue_discount
      total_revenue_after += tran.revenue_after

      val ticket_infos = JsonExt.gson.fromJson(tran.ticket_info, classOf[Array[TicketInfo]])
      for (i <- 0 to ticket_types.length - 1) {
        for (ticket_info <- ticket_infos) {
          if (ticket_info.id_ticket_type == ticket_types(i).id) {
            total_ticket += ticket_info.number
            data_ticket_type(i) += ticket_info.number
          }
        }
      }
    }

    var c = row.createCell(cNum)
    c.setCellValue(total_ticket)
    c.setCellStyle(subHeaderStyle)
    cNum += 1

    for (i <- 0 to ticket_types.length - 1) {
      val c = row.createCell(cNum)
      c.setCellValue(data_ticket_type(i))
      c.setCellStyle(subHeaderStyle)
      c.getCellStyle.setAlignment(HorizontalAlignment.RIGHT)

      cNum += 1
    }

    c = row.createCell(cNum)
    c.setCellValue(Common.currencyFormat(total_revenue_before))
    c.setCellStyle(subHeaderStyle)
    c.getCellStyle.setAlignment(HorizontalAlignment.RIGHT)
    cNum += 1

    c = row.createCell(cNum)
    c.setCellValue(Common.currencyFormat(total_revenue_discount))
    c.setCellStyle(subHeaderStyle)
    c.getCellStyle.setAlignment(HorizontalAlignment.RIGHT)
    cNum += 1

    c = row.createCell(cNum)
    c.setCellValue("")
    c.setCellStyle(subHeaderStyle)
    cNum += 1

    c = row.createCell(cNum)
    c.setCellValue(Common.currencyFormat(total_revenue_after))
    c.setCellStyle(subHeaderStyle)
    c.getCellStyle.setAlignment(HorizontalAlignment.RIGHT)
    cNum += 1

    c = row.createCell(cNum)
    c.setCellValue("")
    c.setCellStyle(subHeaderStyle)
    cNum += 1

    c = row.createCell(cNum)
    c.setCellValue("")
    c.setCellStyle(subHeaderStyle)
    cNum += 1
    /*end subheader*/

    /*start data*/
    trans.foreach { tran =>
      /*init*/
      rNum += 1
      cNum = 0
      row = sheet.createRow(rNum)

      var c = row.createCell(cNum)
      c.setCellValue(Common.mapTransactionStatus(tran.status))
      cNum += 1

      c = row.createCell(cNum)
      c.setCellValue("#" + tran.id)
      cNum += 1

      c = row.createCell(cNum)
      c.setCellValue(Common.convertTimestamp(tran.time_create, "EEE dd/MM/yyyy hh:mm:ss"))
      cNum += 1

      c = row.createCell(cNum)
      c.setCellValue(Common.convertTimestamp(tran.time_payment_success, "EEE dd/MM/yyyy hh:mm:ss"))
      cNum += 1

      c = row.createCell(cNum)
      c.setCellValue(tran.name)
      cNum += 1

      c = row.createCell(cNum)
      c.setCellValue(tran.email)
      cNum += 1

      c = row.createCell(cNum)
      c.setCellValue(tran.phone)
      cNum += 1

      val ticket_infos = JsonExt.gson.fromJson(tran.ticket_info, classOf[Array[TicketInfo]])
      var sum_ticket = 0
      var seat_info_string = ""
      ticket_infos.foreach { ticket_info =>
        sum_ticket += ticket_info.number
        val seat_infos = JsonExt.gson.fromJson(ticket_info.seats_info, classOf[Array[Seat]])
        if (seat_infos != null) {
          seat_infos.foreach(seat_info => if (seat_info_string.length == 0) seat_info_string += seat_info.seat_name_vi else seat_info_string += "," + seat_info.seat_name_vi)
        }
      }

      c = row.createCell(cNum)
      c.setCellValue(sum_ticket)
      cNum += 1

      ticket_types.foreach { ticket_type =>
        val t = ticket_infos.filter(t => t.id_ticket_type == ticket_type.id)
        c = row.createCell(cNum)
        if (t.length > 0) {
          c.setCellValue(t(0).number)
          c.getCellStyle.setAlignment(HorizontalAlignment.RIGHT)
        } else {
          c.setCellValue(0)
          c.getCellStyle.setAlignment(HorizontalAlignment.RIGHT)
        }
        cNum += 1
      }

      c = row.createCell(cNum)
      c.setCellValue(tran.revenue_before)
      c.setCellStyle(numberStyle)
      cNum += 1

      c = row.createCell(cNum)
      c.setCellValue(tran.revenue_discount)
      c.setCellStyle(numberStyle)
      cNum += 1

      c = row.createCell(cNum)
      c.setCellValue(seat_info_string)
      cNum += 1

      var voucher_code = ""
      if (tran.apply_voucher != "") {
        val applied_voucher = JsonExt.gson.fromJson(tran.apply_voucher, classOf[Array[AppliedVoucher]])
        if (applied_voucher != null && applied_voucher.length > 0) {
          voucher_code = applied_voucher(0).voucher_code
        }
      }
      c = row.createCell(cNum)
      c.setCellValue(voucher_code)
      c.setCellStyle(dataCellStyle)
      cNum += 1

      c = row.createCell(cNum)
      c.setCellValue(tran.revenue_after)
      c.setCellStyle(numberStyle)
      cNum += 1

      c = row.createCell(cNum)
      c.setCellValue(Common.mapPaytype(tran.pay_type))
      cNum += 1

      c = row.createCell(cNum)
      c.setCellValue(tran.note)
      c.setCellStyle(dataCellStyle)
      cNum += 1

      val transaction_ticket = tickets.filter(ticket => ticket.id_order == tran.id)
      var count_checked = 0
      transaction_ticket.foreach { tt =>
        if (tt.checkin_status == 1) count_checked += 1
      }

      c = row.createCell(cNum)
      c.setCellValue(count_checked + "/" + transaction_ticket.length)
      c.setCellStyle(dataCellStyle)
      cNum += 1

      ///////////////
      val eventRequireInfoAnswers = eventRequireInfoAnswerDAO.getEventRequireInfoAnswerByOrderId(tran.id);
      eventRequireInfos.foreach { require_info =>
        eventRequireInfoAnswers.foreach { answer =>
          if (require_info.id == answer.event_require_info_id) {
            c = row.createCell(cNum)
            c.setCellValue(answer.answer);
            cNum += 1
          }
        }
      }
    }

    wb.write(fileOut)
    fileOut.close()
    Ok.sendFile(content = file, fileName = _ => file_name)
  }

  def exportReportByTicket = Action { request =>
    val is_admin = request.queryString.get("is_admin").get(0).toInt
    val event_id = request.queryString.get("event_id").get(0).toLong
    val start_date = request.queryString.get("start_date").get(0).toLong
    val end_date = request.queryString.get("end_date").get(0).toLong + 24 * 60 * 60 * 1000
    val ticket_type = request.queryString.get("ticket_type").get(0).toLong
    val pay_type = request.queryString.get("pay_type").get(0).toString
    val status = request.queryString.get("status").get(0).toInt

    val trans = transactionDAO.getTransactionsWithFilters(is_admin, event_id, start_date, end_date, pay_type, 4, ticket_type)
    val ticket_types = evenTicketTypeDAO.getTicketByEvent(event_id)
    var data_ticket_type = Array.fill(ticket_types.length) {
      0
    }

    var header_name_vi = Array[String]("Trạng thái", "Đơn hàng", "Giờ đặt", "Tên", "Email", "Số điện thoại", "Địa chỉ")
    ticket_types.foreach { ticket_type =>
      header_name_vi = header_name_vi ++ Array[String](ticket_type.name_vi)
    }
    header_name_vi = header_name_vi ++ Array[String]("Thông tin ghế", "Voucher Code", "Giảm giá", "Thành tiền", "Phương thức", "Ghi chú")

    var header_name_en = Array[String]("Status", "Order ID", "Purchasing Date and Time", "Payment Date", "Name", "Email", "Phone number", "Ticket Quantity")
    ticket_types.foreach { ticket_type =>
      header_name_en = header_name_en ++ Array[String](ticket_type.name_en)
    }
    header_name_en = header_name_en ++ Array[String]("Subtotal", "Discount Value", "Seat Info", "Voucher Code / Promotion", "Grand Total", "Payment Method", "Note")

    var subheader_name_vi = Array[String]("")
    var subheader_name_en = Array[String]("TOTAL", "", "", "", "", "", "")

    val file_name = "report" + Common.convertTimestamp(System.currentTimeMillis(), "ddMMyyyy") + ".xlsx"
    val file = new File(new File(".").getAbsolutePath() + "/" + file_name)
    val fileOut = new FileOutputStream(file)
    val wb = new XSSFWorkbook
    val sheet = wb.createSheet("Sheet1")
    var rNum = 0
    var row = sheet.createRow(rNum)
    var cNum = 0
    var cell = row.createCell(cNum)


    /*start data*/
    trans.foreach { tran =>
      /*init*/
      val ticket_infos = JsonExt.gson.fromJson(tran.ticket_info, classOf[Array[TicketInfo]])
      ticket_infos.foreach { ticket_info =>
        val seat_infos = JsonExt.gson.fromJson(ticket_info.seats_info, classOf[Array[Seat]])

        seat_infos.foreach { seat_info =>
          val ticket_type = ticket_types.filter(tt => tt.id == seat_info.ticket_type_id)
          if (ticket_type.length > 0) {
            cell = row.createCell(cNum)
            cell.setCellValue(ticket_type(0).name_vi)
            cNum += 1

            cell = row.createCell(cNum)
            cell.setCellValue(seat_info.seat_name_vi)
          }
          rNum += 1
          cNum = 0
          row = sheet.createRow(rNum)
        }
      }
    }

    wb.write(fileOut)
    fileOut.close()
    Ok.sendFile(content = file, fileName = _ => file_name)
  }

  def createInvoiceNumber(): String = {
    "DP" + "%0.7d".format()
  }
}
