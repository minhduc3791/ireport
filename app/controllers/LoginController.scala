package controllers

import java.io.ByteArrayOutputStream
import java.net.{URLDecoder, URLEncoder}

import javax.inject._
import javax.xml.bind.JAXBContext
import javax.xml.transform.stream.StreamResult
import dao._
import helpers.{Common, JsonExt}
import models._
import org.mindrot.jbcrypt.BCrypt
import play.api.db.Database

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.mvc._
import services.PayooService

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */

case class UserForm(var username: String, var password: String)

class LoginController @Inject()(ws: WSClient, val controllerComponents: ControllerComponents, db: Database) extends BaseController {
  val userDAO: UserDAO = new UserDAO(db, controllerComponents);

  def index = Action { request =>
    Ok(views.html.login(""))
  }

  def doLogin = Action { request =>
    //        val admins = Array[String]("khoatang", "du.duong", "hanh.do")
//    val usernames = Array[String]("khoatang", "du.duong", "hanh.do", "van.vo", "box.office1", "box.office2")
//    val normalUsers = Array[String]("client.01")

//    if (usernames.indexOf(u.username) > -1 && u.password == "123654") {
//      Ok(JsonExt.toRawJson("success")).withSession("username" -> u.username, "isAdmin" -> "1", "user_id" -> "9999")
//    } else {
//      val user = userDAO.verifyUserName(u.username);
//      if (user != null) {
////        println("-----------------login 44-----------------")
////        println(user.password)
////        println(BCrypt.hashpw("123654", BCrypt.gensalt()))
//        println(BCrypt.checkpw(u.password, user.password));
//        if (BCrypt.checkpw(u.password, user.password)) {
//          println("51111111");
//          Ok(JsonExt.toRawJson("success")).withSession("username" -> u.username, "isAdmin" -> "1", "user_id" -> user.id.toString);
//        }else{
//          Ok(JsonExt.toRawJson("fail"))
//        }
//      }else{
//        Ok(JsonExt.toRawJson("fail"))
//      }
//    }
    //        if(usernames.indexOf(u.username) > -1 && u.password == "123654"){
    //            Ok(JsonExt.toRawJson("success")).withSession("username" -> u.username, "isAdmin" -> "1")
    //        } else if(normalUsers.indexOf(u.username) > -1 && u.password == "dpclient_"){
    //            Ok(JsonExt.toRawJson("success")).withSession("username" -> u.username)
    //        } else {
    //            Ok(JsonExt.toRawJson("fail"))
    //        }

    val u = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[UserForm])
    val user = userDAO.verifyUserName(u.username);
    if (user != null) {
      println(BCrypt.checkpw(u.password, user.password));
      if (BCrypt.checkpw(u.password, user.password)) {
        Ok(JsonExt.toRawJson("success")).withSession("username" -> u.username, "isAdmin" -> "1", "user_id" -> user.id.toString, "permission_group_id" -> user.permission_group_id.toString);
      }else{
        Ok(JsonExt.toRawJson("fail"))
      }
    }else{
      Ok(JsonExt.toRawJson("fail"))
    }


  }

  def doLogout(event_id: Long) = Action { request =>
    Redirect("transaction/report/" + event_id.toString).withNewSession
  }
}
