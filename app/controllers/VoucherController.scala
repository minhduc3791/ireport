package controllers

import java.io.ByteArrayOutputStream
import java.net.{URLDecoder, URLEncoder}
import javax.inject._
import javax.xml.bind.JAXBContext
import javax.xml.transform.stream.StreamResult

import dao._
import helpers.{Common, JsonExt}
import models._
import play.api.db.Database

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.mvc._
import services.PayooService

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */

class VoucherController @Inject() (ws: WSClient, val controllerComponents: ControllerComponents, db: Database) extends BaseController {
    val voucherDAO: VoucherDAO = new VoucherDAO(db, controllerComponents)

    def claimVoucher = Action { request =>
        val event_id = request.queryString.get("event_id").get(0).toInt
        val email = request.queryString.get("email").get(0)
        val phonenumber = request.queryString.get("phonenumber").get(0)

        val result = new models.Result(1, "", "")
        val voucher = voucherDAO.getFreeVoucher(event_id, email, phonenumber)

        if(voucher == null){
            result.code = -1
            result.data = "Run out of voucher code"
        } else {
            result.data = voucher.voucher_code
        }

        Ok(JsonExt.toRawJson(result))
    }
}
