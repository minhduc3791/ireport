package controllers

import javax.inject._

import dao.{SeatDAO, VoucherDAO}
import helpers.JsonExt
import models.Seat
import play.api.db.Database
import play.api.libs.ws.WSClient
import play.api.mvc._
import services.PayooService

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */

@Singleton
class SeatController @Inject()(ws: WSClient, db: Database, val controllerComponents: ControllerComponents)extends BaseController {
    val seatDAO: SeatDAO = new SeatDAO(db, controllerComponents)

    def getSeatMap = Action { request =>
        val event_id = request.queryString.get("event_id").get(0).toLong
        val seat_map_id = request.queryString.get("seat_map_id").get(0).toLong
        val seats = seatDAO.getSeatsBySeatMapId(seat_map_id, event_id)
        val result = new models.Result(-1, JsonExt.toRawJson(seats), "")

        Ok(JsonExt.toRawJson(result))
    }

    def updateSeatInfo = Action { request =>
        val seat = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[Seat])
        seatDAO.updateSeataInfo(seat)
        val result = new models.Result(-1, "", "")

        Ok(JsonExt.toRawJson(result))
    }

    def lockSeat(seat_id: Long, event_id: Long, seat_map_id: Long) = Action { request =>
        val event_id = request.queryString.get("event_id").get(0).toLong
        val seat_map_id = request.queryString.get("seat_map_id").get(0).toLong
        val seat_id = request.queryString.get("seat_id").get(0).toLong

        val seat = seatDAO.getSeat(seat_id, seat_map_id, event_id)
        val result = new models.Result(-1, "Error", "")
        if(seat != null && seat.is_locked == 0){
            val now = System.currentTimeMillis()
            seatDAO.lockSeat(seat_id, now)
            result.code = 1
            result.data = "Success"
        }
        Ok(JsonExt.toRawJson(result))
    }

    def unlockSeat(seat_id: Long, event_id: Long, seat_map_id: Long) = Action { request =>
        val event_id = request.queryString.get("event_id").get(0).toLong
        val seat_map_id = request.queryString.get("seat_map_id").get(0).toLong
        val seat_id = request.queryString.get("seat_id").get(0).toLong

        val seat = seatDAO.getSeat(seat_id, seat_map_id, event_id)
        val result = new models.Result(-1, "Error", "")

        if(seat != null){
            seatDAO.unlockSeat(seat_id)
            result.code = 1
            result.data = "Success"
        }
        Ok(JsonExt.toRawJson(result))
    }
}
