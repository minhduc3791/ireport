package controllers

import java.io.ByteArrayOutputStream
import java.net.{URLDecoder, URLEncoder}
import javax.inject._
import javax.xml.bind.JAXBContext
import javax.xml.transform.stream.StreamResult

import dao._
import helpers.{Common, JsonExt}
import models._
import play.api.db.Database

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.mvc._
import services.PayooService

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */


class SendgridController @Inject() (ws: WSClient, val controllerComponents: ControllerComponents, db: Database) extends BaseController {
    val sendgridEventNotiDAO: SendgridEventNotiDAO = new SendgridEventNotiDAO(db, controllerComponents)

    def sendgridNotify = Action { request =>
        println(request.body.asJson.get.toString)
//        var x = """[{"email":"van.dong@amberstone.com.vn","timestamp":1547548331,"ip":"64.233.173.23","sg_event_id":"YQu2V9NcS-6kNj_wJXN25g","sg_message_id":"m8FcEB_3SmC8WINVg0a2pQ.filter0089p3mdw1-23528-5C3D6CF9-3.0","category":["1547529455436","payment_success"],"useragent":"Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko Firefox/11.0 (via ggpht.com GoogleImageProxy)","event":"open"}]"""
//        var sendgridEventNotis = JsonExt.gson.fromJson(x, classOf[Array[SendgridEventNoti]])
//        sendgridEventNotis.foreach{s =>
//            sendgridEventNotiDAO.insertSendgridEventNoti(s)
//        }

        Ok("")
    }
}
