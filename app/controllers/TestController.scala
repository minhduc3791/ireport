package controllers

import java.io.ByteArrayOutputStream
import java.net.URLEncoder

import javax.inject._
import javax.xml.bind.JAXBContext
import javax.xml.transform.stream.StreamResult
import helpers.Common
import models.{Article, Artist, CustomerPayoo, OrderPayoo}
import org.mindrot.jbcrypt.BCrypt
import play.api.libs.json._
import play.api.libs.functional.syntax._

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.libs.ws.{EmptyBody, WSAuthScheme, WSClient, WSRequest, WSResponse}
import play.api.mvc._

import scala.collection.mutable.ListBuffer
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */

@Singleton
class TestController @Inject() (ws: WSClient, val controllerComponents: ControllerComponents) extends BaseController  {
    val _SecretKey = "ee655db230291d5c53bcd5170c0f257e"
    implicit val articleWrites = new Writes[Article] {
        def writes(article: Article) = Json.obj(
            "article_id" -> article.article_id,
            "article_bb_id" -> article.article_bb_id,
            "title" -> article.title,
            "author" -> article.author,
            "link" -> article.link,
            "content" -> article.content,
            "publish_date" -> article.publish_date
        )
    }

    implicit val articleReads: Reads[Article] = (
        (JsPath  \ "article_id").read[Long] and
            (JsPath  \ "article_bb_id").read[Long] and
            (JsPath  \ "title").read[String] and
            (JsPath  \ "author").read[String] and
            (JsPath  \ "link").read[String] and
            (JsPath  \ "content").read[String] and
            (JsPath  \ "publish_date").read[String]
        )(Article.apply _)



    
    def viewArticle = Action{
        val url = "https://dreamspass.tech:9117/chart/get/hot-100"
        val user = "bb-vietnam"
        val password = "14aa03e6"
        val request: WSRequest = ws.url(url)//.withAuth(user, password, WSAuthScheme.BASIC)
        val futureResult: Future[WSResponse] = request.get()
        val result = Await.result(futureResult, 10 second)
        /*val items: scala.xml.NodeSeq = result \\ "channel" \\ "item"
        var articles = new ListBuffer[Article]
        for(item <- items){
            val article_bb_id = (item \\ "guid" headOption).map(_.text).map { guid =>
                val x = guid.trim
                x.substring(x.length - 7, x.length - 1)
            }.getOrElse{
                ""
            }

            val title = (item \\ "title" headOption).map(_.text).map { title =>
                title
            }.getOrElse{
                ""
            }

            val author = (item \\ "author" headOption).map(_.text).map { author =>
                author
            }.getOrElse{
                ""
            }

            val link = (item \\ "link" headOption).map(_.text).map { link =>
                link.trim
            }.getOrElse{
                ""
            }

            val content = (item \\ "description" headOption).map(_.text).map { description =>
                description.trim
            }.getOrElse{
                ""
            }

            val publish_date = (item \\ "pubDate" headOption).map(_.text).map { publish_date =>
                publish_date.trim
            }.getOrElse{
                ""
            }

            val a = new Article(-1, article_bb_id.toLong, title, author, link, content, publish_date)
            articles += a
        }*/

        Ok("")
    }

    def viewArtist = Action{
        val url = "http://synd.bbthr.com/get/bb-vietnam/bb-vietnam-artist-100"
        val user = "bb-vietnam"
        val password = "14aa03e6"
        val request: WSRequest = ws.url(url).withAuth(user, password, WSAuthScheme.BASIC)
        val futureResult: Future[scala.xml.NodeSeq] = request.get().map {
            response =>
                response.xml
        }
        val result = Await.result(futureResult, 10 second)
        val items: scala.xml.NodeSeq = result \\ "items" \\ "item"
        var artists = new ListBuffer[Artist]
        for(item <- items){
            val rank = Common.getXMLValue(item, "rank")
            val artist_name = Common.getXMLValue(item, "artist_name")
            val h_peak_rank = Common.getXMLValue(item \\ "history", "peak_rank")
            val h_last_week = Common.getXMLValue(item \\ "history", "last_week")
            val h_weeks_on_chart = Common.getXMLValue(item \\ "history", "weeks_on_chart")
            val image = Common.getXMLValue(item \\ "images" \\ "sizes" \\ "original", "Name")

            val a = new Artist(rank.toLong, artist_name, h_peak_rank.toLong, h_last_week.toLong, h_weeks_on_chart.toLong, image)
            artists += a
        }

        Ok(views.html.billboard_index()(views.html.artist_view("Purpose Media's Tool", artists.toList)))
    }

    def generateQr = Action { implicit request =>
        Ok(views.html.billboard_index()(views.html.qr_view("Purpose Media's Tool", "")))
    }

    def readQr = Action { implicit request =>
        val qrInfo = "pikachupukachi"
        val qrCode = Common.generate(qrInfo)

        Ok(views.html.billboard_index()(views.html.qr_reader("Purpose Media's Tool", qrCode)))

    }

    def hash(text: String) = Action { request =>
        Ok(BCrypt.hashpw(text, BCrypt.gensalt()))
    }
}
