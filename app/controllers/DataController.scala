package controllers

import java.io.ByteArrayOutputStream
import java.net.{URLDecoder, URLEncoder}
import javax.inject._
import javax.xml.bind.JAXBContext
import javax.xml.transform.stream.StreamResult

import dao._
import helpers.{Common, JsonExt}
import models._
import play.api.db.Database

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.mvc._
import services.PayooService

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */

case class CityData(var id: String,  var name: String, var type_city: String, var data: String)
case class DistrictData(var id: String,  var name: String, var type_district: String, var data: String)

class DataController @Inject() (ws: WSClient, val controllerComponents: ControllerComponents, db: Database) extends BaseController {
    val districtDAO: DistrictDAO = new DistrictDAO(db, controllerComponents)
    val wardDAO: WardDAO = new WardDAO(db, controllerComponents)
    val cityDAO: CityDAO = new CityDAO(db, controllerComponents)

    def getFullData = Action { request =>
        val cities = cityDAO.getCities()
        val citiesData = new Array[CityData](cities.length)
        var count = 0

        for(city <- cities){
            val cityData = new CityData(city.id, city.name, city.type_city, getDataOfCity(city.id))

            citiesData(count) = cityData
            count += 1
        }

        Ok(JsonExt.toRawJson(citiesData))
    }

    def getDataOfCity(city_id: String) = {
        val districts = districtDAO.getDistrictByCityId(city_id)
        val districtsData = new Array[DistrictData](districts.length)
        var wards: List[Ward] = Nil
        var count = 0

        for(district <- districts){
            val districtData = new DistrictData(district.id, district.name, district.type_district, JsonExt.toRawJson(wardDAO.getDistrictByCityId(district.id)))

            districtsData(count) = districtData
            count += 1
        }

        JsonExt.toRawJson(districtsData)
    }
}
