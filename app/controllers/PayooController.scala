package controllers

import java.io.ByteArrayOutputStream
import java.net.{URLDecoder, URLEncoder}
import javax.inject._
import javax.xml.bind.JAXBContext
import javax.xml.transform.stream.StreamResult

import dao._
import helpers.{Common, JsonExt}
import models._
import play.api.db.Database

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.mvc._
import services.PayooService

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */

case class RequestPayoo(var data: String, var checksum:String, var refer: String, var restrict: String, var method: String, var bank: String)
case class ResponseOrder(var order_id: Long, var order_no: String, var amount: String, var payment_code: Option[String], var expiry_date: String, var token: String, var payment_url: String)
case class ResponsePayoo(var result: String, var order: ResponseOrder)

class PayooController @Inject() (ws: WSClient, val controllerComponents: ControllerComponents, db: Database) extends BaseController  {
    private val _SecretKey = "e2c76845e3abc589d79eca0533ad18e4"
    private val ShopName = "DreamsPass"
    private val ShopId = 1220L
    private val ShopTitle = "DreamsPass"
    private val APIUsername = "DreamsPass_BizAPI"
    private val APIPassword = "dt0UwccgjFdoN9CN"
    private val APISignature = "k7iqdsx5jmOJ+SJX2qbrcuv/dd5qkfEvn0QNj2ToDDNiP3NEhwZdKGtLHRNZ2J02"
    val transactionSuccessDAO: TransactionSuccessDAO = new TransactionSuccessDAO(db, controllerComponents)
    val transactionDetailDAO: TransactionDetailDAO = new TransactionDetailDAO(db, controllerComponents)
    val transactionDAO: TransactionDAO = new TransactionDAO(db, controllerComponents)
    val payooService: PayooService = new PayooService(ws, db, controllerComponents)
    val evenTicketTypeDAO: EventTicketTypeDAO = new EventTicketTypeDAO(db, controllerComponents)
    val seatDAO: SeatDAO = new SeatDAO(db, controllerComponents)


    def createPayooOrder = Action { implicit r =>
        val order_no = r.queryString.get("order_no").get(0)
        val username = r.queryString.get("username").get(0)
        val phonenumber = r.queryString.get("phonenumber").get(0)
        val address = r.queryString.get("address").get(0)
        val email = r.queryString.get("email").get(0)
        val amount = r.queryString.get("amount").get(0)


        val redirectUrl = ""//payooService.createOrder(order_no, amount, username, phonenumber, address, email)

        Ok(redirectUrl)
    }

    def payooCallback = Action { implicit r =>
        val session = r.queryString.get("session").get(0)
        val order_no = r.queryString.get("order_no").get(0)
        val status = r.queryString.get("status").get(0)
        val checksum = r.queryString.get("checksum").get(0)
        if(!Common.generate("SHA-512", _SecretKey + session + "." + order_no + "." + status).equalsIgnoreCase(checksum))
            Ok("Error: not valid signature")
        else {
            //do update transaction status
            val client_success = "http://dreamspass.vn/demo/#/success?order_no=" + order_no + "&session=" + session + "&status=" + status + "&checksum=" + checksum
            if(status == 1) {
                println("- success: " + order_no  + " _ at: " + System.currentTimeMillis())
                transactionDAO.updateTransactionSuccess(order_no.toLong)
                val transaction = transactionDAO.getTransactionsById(order_no.toLong)
                transactionDAO.updateTransactionSuccess(transaction.id)
                val tickets_info = JsonExt.gson.fromJson(transaction.ticket_info, classOf[Array[TicketInfo]])
                for(ticket_info <- tickets_info) {
                    evenTicketTypeDAO.buyTicket(ticket_info.id_ticket_type, ticket_info.number)
                    val seats_info = JsonExt.gson.fromJson(ticket_info.seats_info, classOf[Array[Seat]])
                    if(seats_info.length > 0) {
                        for (seat_info <- seats_info) {
                            seatDAO.buySeat(seat_info.seat_id)
                            println("+ locked: " + seat_info.seat_id)
                        }
                    }
                }
                Redirect(client_success)
            } else {
                Redirect("http://dreamspass.vn/")
            }
        }
    }


    def payooNotify = Action { implicit request =>
        val req = request.body.asFormUrlEncoded.get.keySet.head
        val response = scala.xml.XML.loadString(req.substring(11, req.length))
        val data = scala.xml.XML.loadString(java.util.Base64.getDecoder.decode(URLDecoder.decode(Common.getXMLValue(response, "Data"), "UTF-8")).map(_.toChar).mkString)
        val signature = Common.getXMLValue(response, "Signature")
        val key_fields = Common.getXMLValue(response, "KeyFields")
        var verify_data = _SecretKey

        //note : if transaction payment method == STORE => map field data have camel case format => need to check
        key_fields.split('|').foreach( key => verify_data += "|" + mapFieldData(key, data))
        println("receive_" + mapFieldData("OrderNo", data) + "_" + System.currentTimeMillis())
        if(!Common.generate("SHA-512", verify_data).equalsIgnoreCase(signature))
            Ok("Error: not valid signature")
        else {
            //do update transaction status
            //sold seat, sold ticket
            if(mapFieldData("State", data).equalsIgnoreCase("PAYMENT_RECEIVED")) {
                println("- success: " + mapFieldData("OrderNo", data)  + " _ at: " + System.currentTimeMillis())
                val transaction = transactionDAO.getTransactionsById(mapFieldData("OrderNo", data).toLong)
                transactionDAO.updateTransactionSuccess(transaction.id)
                val tickets_info = JsonExt.gson.fromJson(transaction.ticket_info, classOf[Array[TicketInfo]])
                for(ticket_info <- tickets_info) {
                    evenTicketTypeDAO.buyTicket(ticket_info.id_ticket_type, ticket_info.number)
                    val seats_info = JsonExt.gson.fromJson(ticket_info.seats_info, classOf[Array[Seat]])
                    if(seats_info.length > 0) {
                        for (seat_info <- seats_info) {
                            seatDAO.buySeat(seat_info.seat_id)
                            println("+ locked: " + seat_info.seat_id)
                        }
                    }
                }
                Ok("NOTIFY_RECEIVED")
            } else {
                Ok("PROCESSING")
            }
        }
    }

    def mapFieldData(field: String, node: scala.xml.NodeSeq): String = {
        field match {
            case "PaymentMethod" => Common.getXMLValue(node, "PaymentMethod")
            case "State" => Common.getXMLValue(node, "State")
            case "Session" => Common.getXMLValue(node, "session")
            case "BusinessUsername" => Common.getXMLValue(node, "username")
            case "ShopID" => Common.getXMLValue(node, "shop_id")
            case "ShopTitle" => Common.getXMLValue(node, "shop_title")
            case "ShopDomain" => Common.getXMLValue(node, "shop_domain")
            case "ShopBackUrl" => Common.getXMLValue(node, "shop_back_url")
            case "OrderNo" => Common.getXMLValue(node, "order_no")
            case "OrderCashAmount" => Common.getXMLValue(node, "order_cash_amount")
            case "StartShippingDate" => Common.getXMLValue(node, "order_ship_date")
            case "ShippingDays" => Common.getXMLValue(node, "order_ship_days")
            case "OrderDescription" => URLDecoder.decode(Common.getXMLValue(node, "order_description"), "UTF-8")
            case "NotifyUrl" => Common.getXMLValue(node, "notify_url")
            case "PaymentExpireDate" => Common.getXMLValue(node, "validity_time")
            case _ => ""
        }
    }
}
