package controllers

import javax.inject._

import helpers.JsonExt
import play.api.db.Database
import play.api.libs.ws.WSClient
import play.api.mvc._

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */

case class Login(var username: String, var password: String)
@Singleton
class HomeController @Inject()(ws: WSClient, db: Database, val controllerComponents: ControllerComponents)extends BaseController {

    /**
      * Create an Action to render an HTML page with a welcome message.
      * The configuration in the `routes` file means that this method
      * will be called when the application receives a `GET` request with
      * a path of `/`.
      */
    var count = 0
    def login = Action {
        count = count + 1
        Ok(views.html.login(count.toString))
    }

    def doLogin = Action { request =>
        val u = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[Login])
        Redirect("/test/article")
        /*if((t.username.equals("khoatang") && t.password.equals("duyetformchoemnghi")) || (t.username.equals("admin") && t.password.equals("ducdeptrai"))) {
            Redirect(controllers.routes.TransactionController.report(30)).withSession("username" -> t.username)
        } else {
            Redirect(controllers.routes.TransactionController.report(30)).withSession("username" -> t.username)
        }*/
    }
}
